(* There you will find page for full description of node in timeline.
 * Below the description some space for user's comments to the node
 * should be.
 *)

{shared{
  open Eliom_content.Html5
  open Eliom_content.Html5.F
  open Printf
  open Helpers
  open Types

  let (>|=) = Lwt.(>|=)
}}

{client{
open ClientHelpers
open Ojquery

let _bad id = Firebug.console##log (Js.string @@ sprintf "Can't get element by id '%s'" id)
let prompt_line ?text ~title ~ok =
  let ans = Dom_html.window##prompt
    (Js.string title, Js.string @@ match text with Some x -> x | None -> "") in
    Js.Opt.iter ans (fun text -> ok (Js.to_string text))

}}


let dummy_add_comment_service = Eliom_service.Http.post_coservice'
  ~name:"set_data"
  ~post_params:(Eliom_parameter.(int64 "id" ** string "text")) ()

{shared{
type operation_mode = [ `Guest | `User of int64 | `Moderator of int64 ]
let event_comments_real_holder_id = "event_comments_real_holder_id"
let event_description_holder_id   = "event_description_holder"
}}

let add_comment_rpc
  : (userId_t*eventId_t*(commentId_t option)*string, bool) Eliom_pervasives.server_function
  = server_function Json.t<userId_t*eventId_t*(commentId_t option)*string>
    (fun (userId, nodeId, commentId, text) ->
       match commentId with
       | Some parentId ->
         printf "adding comment by %Ld to node %Ld as subcomment of %Ld with text '%s'\n%!"
           userId nodeId parentId text;
         lwt () = Comments_db.insert_subcomment ~text ~nodeId ~parentId ~userId in
         Lwt.return true
       | None          ->
         lwt () = Comments_db.insert_comment ~text ~nodeId ~userId in
         Lwt.return true
    )

let comments_for_node_rpc
  : (eventId_t, CommentWithUINFO.t list) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t> Comments_db.comment_data_for_event

{client{
let refresh_comments_data () =
  with_element_by_id_exn "refresh_comments_btn" ~ok:(fun ddd -> ddd |> jQelt |> click |> ignore)
    ~bad:_bad

let add_comment_client ~userId ~eventId ~commentId ~text =
  lwt res = %add_comment_rpc (userId, eventId, commentId, Js.to_string text) in
  let () = refresh_comments_data () in
  Lwt.return res

let make_comment_div ~mode ~eventId padding (o: CommentWithUINFO.t) =
  let open Eliom_content.Html5.D in
  let comment_id = o.CommentWithUINFO.id in
  let comment_div_id = sprintf "node-comment-%Ld" comment_id in

  let answer_form_div = div ~a:[a_class ["node_comment_reply_block"]; a_style "display: none;"]
    [ post_form ~service: %dummy_add_comment_service (fun (id,text) ->
       let area = textarea ~a:[a_class ["node_comment_reply_textarea"]] ~name:text
	 ~value:"comment text here" () in
       let btn = match mode with
         | `Moderator user
         | `User user ->
           let add_comment_clicked _event =
             Dom.preventDefault _event;
             let jsarea = To_dom.of_textarea area in
             let text = jsarea##value in
             Firebug.console##log (text);
             let userId = user.Ukraine_user.uid in
             begin
             lwt res = add_comment_client ~userId ~eventId ~commentId:(Some comment_id) ~text in
             if not res then Firebug.console##log (Js.string "can't add comment")
             else jsarea##value <- Js.string "";
             Lwt.return ()
             end |> Lwt.ignore_result
           in
           raw_input ~a:[a_onclick add_comment_clicked] ~input_type:`Submit ~value:"Comment" ()
         | `Guest -> div []
       in
       [ area
       ; int64_input ~input_type:`Hidden ~name:id ~value:comment_id ()
       ; btn
       ]) ()
    ]
  in
  let reply_btn = match mode with
    | `User user
    | `Moderator user ->
      let reply_clicked _ =
          Firebug.console##log (Js.string @@ sprintf "reply on comment %Ld" comment_id);
          Manip.SetCss.display answer_form_div "block";
          Firebug.console##log (Js.string @@ sprintf "reply on comment %Ld" comment_id);
      in
      div ~a:[a_class ["button";"node_comment_reply_btn"]; a_onclick reply_clicked]
		[pcdata "Reply"]
    | `Guest -> div []
  in
  let comment_header =
    let nick = sprintf "%s %s" o.CommentWithUINFO.fn o.CommentWithUINFO.ln in
    div ~a:[a_class ["node_comment_header"]]
      [ div ~a:[a_class ["node_comment_avatar"]]
	  [ img ~src:(Xml.uri_of_string Consts.no_image_url) ~alt:"alt" () ]
      ; div ~a:[a_class ["node_comment_nick"]] [pcdata nick]
      ]
  in
  div ~a:[ a_style @@ sprintf "margin-left: %dpx;" (30*padding)
	 ; a_id comment_div_id; a_class ["node_comment"]
	 ]
      [ div ~a:[a_class ["node_comment_holder"]]
          (* comment static data here *)
	  [ comment_header
	  ; div ~a:[a_class ["node_comment_text"]]  [ pcdata o.CommentWithUINFO.text ]
	  ; reply_btn
	  ]
      ; answer_form_div
      ]

let generate_comments_divs ~mode ~eventId comments : _ elt list =
  let (tree, non_parsed_part) = Path.make_tree_of_nodes (fun o -> o.CommentWithUINFO.path) comments in

  let rec to_divs acc depth : CommentWithUINFO.t Path.tree -> _ elt list = function
    | Path.Node (x,xs) ->
       let r = make_comment_div ~mode ~eventId depth x in
       List.fold_left ~init:(r::acc) xs ~f:(fun acc node -> to_divs acc (depth+1) node )
  in
  List.concat (List.map (List.rev tree) ~f:(to_divs [] 0 ) ) |> List.rev

}}


let questions_and_options_rpc
    : (eventId_t, Types.Question.t list) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t> Events_db.questions_with_options_of_nodeId

let get_node_by_id_rpc : (eventId_t, Types.Event.t) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t> Events_db.event_by_id

let get_event_by_pos_rpc : (int64*timeline_t, Types.Event.t option) Eliom_pervasives.server_function
  = server_function Json.t<int64*timeline_t> (fun (p,id) -> Events_db.get_by_pos p id)

let add_question_rpc: (eventId_t*string, bool) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t*string>
    (fun (nodeId,text) -> Events_db.insert_question ~nodeId ~text)

let remove_question_rpc: (questionId_t, bool) Eliom_pervasives.server_function
  = server_function Json.t<questionId_t> (fun questionId -> Events_db.remove_question ~questionId)

let remove_event_rpc
  : (int64, int64) Eliom_pervasives.server_function
  = server_function Json.t<int64> Events_db.remove_and_get_prev

let add_option_rpc: (optionId_t*string, bool) Eliom_pervasives.server_function
  = server_function Json.t<optionId_t*string>
    (fun (questionId,text) -> Events_db.insert_option ~questionId ~text)

let remove_option_rpc: (optionId_t, bool) Eliom_pervasives.server_function
  = server_function Json.t<optionId_t> (fun optionId -> Events_db.remove_option ~optionId)

let get_user_by_id_rpc : (int64, Ukraine_user.t) Eliom_pervasives.server_function
  = server_function Json.t<int64> Ukraine_user.by_id

let is_moderator_rpc : (int64, bool) Eliom_pervasives.server_function
  = server_function Json.t<int64> (fun userId -> Ukraine_groups.is_moderator ~userId)

{client{

let make_question_div ~refresh ~ismoderator ~eventId q =
  let remove_question_onclicked = fun _event ->
    Dom.preventDefault _event;
    Firebug.console##log (Js.string "remove_question_onclicked");
    let do_confirm = Dom_html.window##confirm (Js.string "Really remove question and its interpretations?")  in
    if Js.to_bool do_confirm then
    begin
    lwt (_:bool) = %remove_question_rpc q.Question.id in
    refresh eventId;
    Lwt.return ()
    end |> Lwt.ignore_result
  in

  let add_option_clicked = fun _event ->
      Dom.preventDefault _event;
      Firebug.console##log (Js.string "add_option_onclicked");
      prompt_line ~title:"Enter new option text" ~text:"" ~ok:begin fun text ->
	Firebug.console##log (Js.string @@ sprintf  "your option value =  '%s'" text);
        (lwt res = %add_option_rpc (q.Question.id, text) in
	 let () = if res then begin
           Firebug.console##log (Js.string "Added new option");
           refresh eventId;
         end else
           Firebug.console##error (Js.string "Server can't add option ");
         in
         Lwt.return ()
	) |> Lwt.ignore_result
       end;
      ()
  in
  let make_option_div (cid,text) =
    let remove_option_clicked = fun _event ->
      Dom.preventDefault _event;
      Firebug.console##log (Js.string "remove_option_onclicked");
      begin
	lwt (_:bool) = %remove_option_rpc cid in
        refresh eventId;
        Lwt.return ()
      end |> Lwt.ignore_result
    in
    div ~a:[a_class ["question_option_div"]]
      (if not ismoderator then [pcdata text]
       else
         [ pcdata text
         ; dummy_img ~a:[a_onclick remove_option_clicked;
		         a_class ["remove_option_btn"]; a_title "Remove option"] ()
         ]
      )
  in
  let buttons = div ~a:[a_class ["question_buttons_div"]] (if not ismoderator then [] else
        [ dummy_img ~a:[a_onclick remove_question_onclicked; a_class ["remove_question_btn"];
			a_title "Remove question"] ()
        ; dummy_img ~a:[ a_onclick add_option_clicked
                       ; a_class ["add_option_btn"]; a_title "Add new option"] ()
        ])
  in
  Lwt.return @@
    div ~a:[a_class ["question_div"]]
        [ dummy_img ~a:[a_class ["question_icon_img"]] ()
        ; div ~a:[a_class ["question_div_text"]] [pcdata q.Question.text]
        ; buttons
        ; br ()
        ; div ~a:[a_class ["question_options_div"]] (List.map q.Question.options ~f:make_option_div)
        ; hr ()
        ]

}}


{client{
(* we don't use Str module because missing primitives *)
let make_description s =
  [Of_dom.of_div @@ Wiki_syntax.xml_of_wiki s]

let rec refresh_event_data ~mode eventId = Lwt.ignore_result begin
  Firebug.console##log (Js.string @@ sprintf "refresh_event_data id=%Ld" eventId);

  lwt curnode = %get_node_by_id_rpc eventId in
  lwt questions = %questions_and_options_rpc eventId in
  let ismoderator = match mode with `Moderator _ -> true | _ -> false in

  lwt question_divs =
    questions
    |> List.map ~f:(make_question_div ~ismoderator ~refresh:(refresh_event_data ~mode) ~eventId)
    |> Lwt_list.map_p (fun x -> x)
  in

  with_element_by_id_exn %event_description_holder_id ~ok:(fun el ->
    let nodes =
	[ div ~a:[a_class ["view_event_title"]] [pcdata @@ curnode.Event.title]
	; br ()
	; div ~a:[a_class ["view_event_description"]]
	     (make_description curnode.Event.text)
        ; hr ()
        ; div ~a:[a_class ["view_event_original_link_holder"]]
                 (match curnode.Event.link with
                  | Some link when String.length link > 0 ->
                    [Unsafe.node "a" ~a:[a_href @@ Xml.uri_of_string link]
                       [pcdata "Link to original news"]]
                  | _ -> []
                 )
        ; hr ()
        ; div ~a:[a_class ["questions_block"]] question_divs
	]
      in
      Manip.replaceChildren (el |> Of_dom.of_element) nodes
  ) ~bad:_bad;
  Lwt.return ()
end

}}

let make_event_links ~ismoderator ~eventId ~nextEventId ~prevEventId tlsort message_div xs =
  let f ev =
    div ~a:[a_class @@ if ev.Event.id=eventId then ["view_event_selected_event"] else []]
      [ a ~a:[a_class ["event_view_link"]] ~service:Ukraine_services.view_event_service
	  [pcdata ev.Event.title] ev.Event.id
      ; br ()
      ; div ~a:[a_class ["event_view_link_time"]] [pcdata @@ string_of_time ev.Event.datetime]
      ; br ()
      ]
  in

  let d = div ~a:[a_class ["view_event_links_heading"]]
      [ (match prevEventId with
        | Some id ->
          a ~a:[a_class ["view_event_prev_day"]] ~service:Ukraine_services.view_event_service
	    [dummy_img ()] id
        | None  -> div ~a:[a_class ["view_event_prev_day_unclickable"]] [dummy_img ()])
      ; message_div
      ; (match nextEventId with
        | Some id ->
          a ~a:[a_class ["view_event_next_day"]] ~service:Ukraine_services.view_event_service
	    [dummy_img ()] id
        | None  -> div ~a:[a_class ["view_event_next_day_unclickable"]] [dummy_img ()])
      ]
  in
  Lwt.return @@ div ~a:[a_class ["event_view_links_holder"]]
    ( d :: (List.map ~f xs) )

let handler mode (eventId: eventId_t) () =
  let ismoderator = match mode with `Moderator _ -> true | _ -> false in
  lwt event = Events_db.get_by_id eventId  in
  let tlsort = timeline_of_id event.Event.timelineId in
  let date = CalendarLib.Calendar.(to_date event.Event.datetime) in
  let dd = CalendarLib.Date.(day_of_month date) in
  let mm = CalendarLib.Date.(int_of_month @@ month date) in
  let yy = CalendarLib.Date.(year date) in
  lwt events = Events_db.by_date tlsort ~yy ~dd ~mm in

  (* next two ids are used to make navigation links (back/forward) *)
  lwt (nextEventId,prevEventId) =
    match events with
    | [] -> Lwt.return (None,None)
    | xs ->
      let right = List.hd_exn events in (* Right on the top *)
      lwt next =
          Events_db.get_by_pos Int64.(add 1L right.Event.position) tlsort
          >|= Helpers.Option.map ~f:(fun x -> x.Event.id)
      in
      (* Left event is drawn on the bottom *)
      let left  = List.last_exn events in
      lwt prev =
        if left.Event.id = timeline_start_event tlsort then Lwt.return None
        else
            Events_db.get_by_pos Int64.(sub left.Event.position 1L) tlsort
            >|= Helpers.Option.map ~f:(fun x -> x.Event.id)
      in
      Lwt.return (next,prev)
  in

  let comments_holder =
    let node_comments_real_holder =
      let refresh_onload = {Dom_html.event Js.t -> unit { fun _ -> refresh_comments_data () }} in
      D.div ~a:[ a_class ["node_comments_real_holder"]; a_id event_comments_real_holder_id
               ; a_onload refresh_onload] []
    in
    let refresh_clicked = {Dom_html.mouseEvent Js.t -> unit { fun _ ->
        Firebug.console##log (Js.string "refresh comments clicked");
        Lwt.ignore_result (
          lwt comments = %comments_for_node_rpc %eventId in
          lwt mode = match %mode with
            | `Guest -> Lwt.return `Guest
            | `User uid      -> %get_user_by_id_rpc uid >|= (fun u -> `User u)
            | `Moderator uid -> %get_user_by_id_rpc uid >|= (fun u -> `Moderator u)
          in
          let eventId = %eventId in
	  let divs = generate_comments_divs ~mode ~eventId comments in
	  Manip.removeChildren %node_comments_real_holder;
          Manip.appendChildren %node_comments_real_holder divs;
          Lwt.return ()
        )
    }} in
    let refresh_button = div [pcdata "Refresh comments"]
      ~a:[a_class ["button"]; a_id "refresh_comments_btn"; a_onclick refresh_clicked ]
    in
    div ~a:[a_class ["node_comments_holder"]]
	[div ~a:[a_class ["node_comments_holder1"]] [refresh_button]
	; hr ()
	; node_comments_real_holder
	]
  in

  let onload_event_description_div = {Dom_html.event Js.t -> unit{ fun _ ->
    refresh_event_data ~mode: %mode %eventId;
  }} in
  let refresh_event_data_btn_clicked = {Dom_html.mouseEvent Js.t -> unit{ fun _event ->
    Dom.preventDefault _event;
    refresh_event_data ~mode: %mode %eventId
  }} in

  let event_comment_root_block = match mode with
    | `Moderator userId
    | `User userId ->
      div ~a:[a_class ["node_comment_root_reply_block"]]
	[ post_form ~service:dummy_add_comment_service (fun (id,text) ->
            let open Eliom_content.Html5.D in
            let area = textarea ~a:[a_class ["node_comment_root_reply_textarea"]]
	        ~name:text ~value:"comment text here" () in
            let add_comment_onclick = {Dom_html.mouseEvent Js.t -> unit { fun _event ->
              Dom.preventDefault _event;
              let jsarea = To_dom.of_textarea %area in
              let text = jsarea##value in
              Firebug.console##log (text);
              begin
              lwt res = add_comment_client ~userId:%userId ~eventId:%eventId ~text ~commentId:None in
              if not res then Firebug.console##log (Js.string "Can't add comment")
              else jsarea##value <- Js.string "";
              Lwt.return ()
              end |> Lwt.ignore_result
            }} in
            [ div ~a:[a_style "float: left;"] [pcdata "Comment node:"]
	    ; br ()
	    ; area
	    ; br ()
	    ; raw_input ~a:[a_onclick add_comment_onclick; a_class ["node_comment_root_submit_btn"]]
                ~input_type:`Submit ~value:"Comment" ()
	    ]) ()
        ]
    | `Guest -> div []
  in

  let add_question_btn_clicked = {Dom_html.mouseEvent Js.t -> unit{ fun _event ->
    Dom.preventDefault _event;
    prompt_line ~title:"Enter new question" ~text:"" ~ok:begin fun text ->
      Firebug.console##log (Js.string @@ sprintf  "your value %s" text);
      (lwt res = %add_question_rpc (%eventId, text) in
      let () = if res then begin
        Firebug.console##log (Js.string "Added");
        refresh_event_data ~mode: %mode %eventId;
      end else begin
        Firebug.console##error (Js.string "Server can't add question");
      end in
      Lwt.return ()) |> Lwt.ignore_result
    end
  }} in
  let delete_event_btn_clicked = {Dom_html.mouseEvent Js.t -> unit{ fun _event ->
    Dom.preventDefault _event;
    match %mode with
    | `Guest  -> Firebug.console##error (Js.string "guests can't remove events")
    | `User _ -> Firebug.console##error (Js.string "Simple users can't remove events")
    | `Moderator _ ->
      (lwt prev = %remove_event_rpc %eventId in
       Lwt.return @@ Eliom_client.exit_to ~service: %Ukraine_services.view_event_service prev ()
      ) |> Lwt.ignore_result
  }} in

  let buttons_bar =
    let make_btn ?title onclick clas =
      let classes = match title with
        | Some  _ -> [clas;"need_tooltip"]
        | None    -> [clas]
      in
      let a = match title with
        | Some t -> [a_onclick onclick; a_class classes; a_title t]
        | None   -> [a_onclick onclick; a_class classes ]
      in
      Some (div ~a [dummy_img () ])
    in
    let make_href ?title ~service clas =
      match title with
      | None       -> a ~a:[a_class [clas]] ~service  [dummy_img()]
      | Some title -> a ~a:[a_class [clas;"need_tooltip"]; a_title title] ~service [dummy_img()]
    in
    let make_tooltips = {Dom_html.event Js.t-> unit{ fun _ ->
        Ojquery.(".need_tooltip" |> js_jQ |> jQelt |> jq_tooltip) }} in
    div ~a:[a_class ["event_buttons_bar"]; a_onload make_tooltips] @@
    List.filter_map (fun f -> f ())
      [ begin fun () -> make_btn refresh_event_data_btn_clicked "refresh_event_data_btn" end
      ; begin fun () ->
        if ismoderator && eventId <> 1L
        then make_btn ~title:"Delete event" delete_event_btn_clicked "delete_event_btn"
        else None
        end
      ; begin fun () ->
        if ismoderator
        then Some (make_href ~title:"Edit event" ~service:Ukraine_services.edit_event_service
                     "edit_event_btn" eventId)
        else None
        end
      ; begin fun () ->
        if ismoderator
        then Some (make_href ~title:"Insert after" ~service:Ukraine_services.add_node_service
                     "insert_event_after_btn" eventId)
        else None
        end
      ; begin fun () ->
        if ismoderator
        then make_btn ~title:"Add question" add_question_btn_clicked "event_toolbar_add_question_btn"
        else None
        end
      ; begin fun () ->
        if ismoderator
        then Some(make_href ~title:"Smart add event" ~service:Ukraine_services.smart_add_service
                    "event_toolbar_smart_add_question_btn" (Types.id_of_timeline tlsort) )
        else None
        end
      ]
  in
  let content_div = div ~a:[a_class ["event_data_holder"]]
    [ buttons_bar
    ; div ~a:[ a_class ["event_description_holder"]
	     ; a_id event_description_holder_id; a_onload onload_event_description_div] []
    ; comments_holder
    ; hr ()
    ; event_comment_root_block
    ]
  in

  lwt content_div =
    let message_div = div ~a:[a_class ["event_view_links_heading_msg"]]
	[ pcdata @@ Types.string_of_date event.Event.datetime ]
    in
    lwt left_side_links = make_event_links ~ismoderator ~prevEventId ~nextEventId
        ~eventId tlsort message_div events in
    Lwt.return @@ div [ left_side_links
                      ; content_div
	              ]
  in
  Lwt.return (match mode with
        | `Moderator user
        | `User user -> Ukraine_container.page ~user [content_div]
        | `Guest     -> Ukraine_container.page [content_div]
  )

let () =
  let onerror  = LoginError.page None in
  let open Ebapp.Session in
  let handler' mode eventId () =
    match mode with
    | `GuestDenied
    | `GuestConnected -> handler `Guest eventId ()
    | `Allowed userId -> handler (`Moderator userId) eventId ()
    | `Denied userId  -> handler (`User userId) eventId ()
  in

  Ebapp.App.register Ukraine_services.view_event_service
    (Ebapp.Page.how_connected_page ~allow:[Ukraine_groups.moderator_group] ~onerror handler')
