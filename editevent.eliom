(* Service handler for adding new events.
 * TODO: refactor code to allow editing of events
 *)
open Printf
open Types

{shared{
open Eliom_content.Html5
open Eliom_content.Html5.F
type datetime_str = string deriving (Json)
let add_event_date_input_id = "add_event_date_input"
let add_event_time_input_id = "add_event_time_input"
}}

{client{
open Printf
open Helpers
open ClientHelpers
open Ojquery
}}

let dummy_service' =
  Eliom_service.Http.post_coservice  ~fallback:Ukraine_services.view_event_service
    ~post_params:Eliom_parameter.(string "title"**string "image" ** string "lenta_text" ** string "lenta_url")
    ()

let update_event_rpc
  : (eventId_t*string*string*string*datetime_str, unit) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t*string*string*string*datetime_str>
    (fun (eventId,title,text,link,datetime_str) ->
       Events_db.update ~text ~link ~title ~datetime_str ~eventId)

let handler userId (eventId: int64) () : _ Lwt.t =
  lwt curevent = Events_db.get_by_id eventId in

  let open Eliom_content.Html5.D in
  let form = post_form ~service:dummy_service' (fun (title,(image,(lenta_text,lenta_url))) ->
    let title_input = string_input ~a:[a_placeholder "title"; a_class ["add_node_line_input"]]
        ~name:title ~input_type:`Text ()
        ~value:curevent.Event.title
    in
    let lenta_input = string_input ~a:[a_placeholder "url"; a_class ["add_node_line_input"]]
        ~name:lenta_url ~input_type:`Url ()
        ~value:(Helpers.Option.get curevent.Event.link ~default:"")
    in
    let text_input = textarea ~a:[a_placeholder "text"; a_class ["edit_event_text_input"]]
        ~name:lenta_text ()
        ~value: curevent.Event.text
    in
    let date_input = string_input ~name:lenta_text ~input_type:`Text ()
        ~a:[ a_placeholder "date"; a_class ["datepicker";"add_node_date_input"]
           ; a_id add_event_date_input_id ]
        ~value:(string_of_date curevent.Event.datetime)
    in
    let time_input = string_input ~name:lenta_text ~input_type:`Text ()
        ~a:[ a_placeholder "time"; a_class ["timepicker";"add_node_time_input"]
           ; a_id add_event_time_input_id ]
        ~value:(string_of_time curevent.Event.datetime)
    in
    let submit_clicked = {Dom_html.mouseEvent Js.t -> unit { fun _event ->
      Dom.preventDefault _event;
      (* TODO: we need to check that date is >parentDate and  < nextDate *)
      let title    = (To_dom.of_input %title_input)##value |>  Js.to_string in
      let text     = (To_dom.of_textarea %text_input)##value |> Js.to_string in (*
      let image    = (To_dom.of_input %image_input)##value |> Js.to_string in *)
      let link     = (To_dom.of_input %lenta_input)##value |> Js.to_string in
      let datetime = sprintf "%s %s"
         ( (To_dom.of_input %date_input)##value |> Js.to_string )
         ( (To_dom.of_input %time_input)##value |> Js.to_string )
      in
      Lwt.ignore_result begin
      lwt () = %update_event_rpc (%eventId, title, text, link, datetime) in
      Eliom_client.exit_to ~service: %Ukraine_services.view_event_service %eventId ();
      Lwt.return ()
      end
    }} in

    [ title_input; br ()
    ; lenta_input; br ()
    ; pcdata "Date"; date_input; pcdata "Time";  time_input; br ()
    ; text_input ; br ()
    ; string_input ~a:[a_class ["button"]; a_onclick submit_clicked ]
        ~input_type:`Button ~value:"Update event" ()
    ]
    ) userId
  in
  let content_div_onload = {Dom_html.event Js.t -> unit { fun _event ->
    (* I don't do this in input.onload event because by some reason is didn't work for me *)
    (* TODO: understand how to do it typesafe *)
    let el = get_element_by_id_exn add_event_date_input_id in
    let date_params: timepicker_options Js.t =
      (* International english format *)
      Js.Unsafe.eval_string "new Object({dateFormat: 'DD-MM-YY'})"
    in

    el |> jQelt |> (jq_datepicker date_params) |> ignore;
    let el = get_element_by_id_exn add_event_time_input_id in
    let time_params: timepicker_options Js.t = Js.Unsafe.obj [||] in
    time_params##step <- Js.string "5";
    time_params##timeFormat <- Js.string "H:i:s";
    el |> jQelt |> (jq_timepicker time_params) |> ignore;
  }} in
  let content_div = div ~a:[a_onload content_div_onload] [ form ] in

  Lwt.return (Ukraine_container.page ~user:userId [ content_div ])

let () = Ebapp.App.register Ukraine_services.edit_event_service
    (Ebapp.Page.connected_page handler)
