
{shared{
  open Eliom_content.Html5
  open Eliom_content.Html5.F
  open Printf
  open Helpers
  open Types
}}

let get_last_id_rpc : (timeline_t, eventId_t) Eliom_pervasives.server_function
  = server_function Json.t<timeline_t> Events_db.get_last_event_id

let handler tlsort () () =
  let onloaded = {Dom_html.event Js.t -> unit{ fun _ ->
      (lwt last = %get_last_id_rpc %tlsort in
      Eliom_client.exit_to ~service: %Ukraine_services.view_event_service last ();
      Lwt.return ()
      ) |> Lwt.ignore_result
    }} in
  let content_div = div ~a:[a_onload onloaded] [pcdata "Trying to redirect..."] in

  Lwt.return @@ Ukraine_container.page [
    content_div
  ]

let ukraine_last_handler = handler Maidan2013
let grf_last_handler = handler GreatRusFirewall

let () =
  Ebapp.App.register Ukraine_services.ukraine_last_service
    (Ebapp.Page.page  ukraine_last_handler)

let () =
  Ebapp.App.register Ukraine_services.grf_last_service
    (Ebapp.Page.page grf_last_handler)
