open Ukraine_db
open Helpers
open Printf
open Types
open Lwt

let events_id_seq = (<:sequence< bigserial "events_id_seq" >>)
let events_table  = (<:table< events
( id                  bigint    NOT NULL DEFAULT(nextval $events_id_seq$),
  timelineId          integer   NOT NULL,
  position            bigint    NOT NULL,
  title               text      NOT NULL,
  text                text      NOT NULL,
  link                text,
  datetime            timestamp NOT NULL
) >>)

let questions_id_seq = (<:sequence< bigserial "questions_id_seq" >>)
let questions_table  = (<:table< questions
( id                  bigint    NOT NULL DEFAULT(nextval $questions_id_seq$),
  eventId             bigint    NOT NULL,
  text                text      NOT NULL
) >>)

let lastId dbh seq =
  lwt () = PGOCaml.prepare dbh ~query:(sprintf "SELECT currval('%s')" seq) () in
  lwt (xs: PGOCaml.row list) = PGOCaml.execute dbh ~params:[] () in
  match List.hd xs with
    | (Some lastid)::_ -> Lwt.return @@ Int64.of_string lastid
    | _  -> failwith (sprintf "Internal error while executing lastId of '%s' " seq)

let sql_escape s = s
(*                   |> Str.global_replace (Str.regexp "\n") "\\n" *)
                   |> Str.global_replace (Str.regexp "'") "''"

let update ~text ~title ~link ~eventId ~datetime_str =
  let query = sprintf "UPDATE events SET text='%s', link='%s', title='%s', datetime='%s' WHERE id=%Ld"
      (sql_escape text) link (sql_escape title)
      (datetime_str |> calendar_of_string |> dbstring_of_calendar)
      eventId
  in
  (*print_endline query;*)
  full_transaction_block (fun dbh ->
    lwt () = PGOCaml.prepare dbh ~query () in
    lwt (_: PGOCaml.row list) = PGOCaml.execute dbh ~params:[] () in
    Lwt.return ()
  )

let insert_after ~text ~title ~link ~parentId ~datetime_str =
  (* TODO: escape strings before inserting them into query *)
  let query = sprintf "SELECT GeneralInsertAfterParentId(%Ld,'%s','%s','%s','%s')"
      parentId (sql_escape title) (sql_escape text) (sql_escape link)
      (datetime_str |> calendar_of_string |> dbstring_of_calendar)
  in
  (* print_endline query; *)
  full_transaction_block (fun dbh ->
  lwt () = PGOCaml.prepare dbh ~query () in
  lwt (xs: PGOCaml.row list) = PGOCaml.execute dbh ~params:[] () in
  match List.hd_exn xs with
  | (Some _) :: _ ->
    lwt cur_id = lastId dbh "events_id_seq" in
    Lwt.return cur_id
  | _ -> failwith @@ sprintf "Can't insert event with parentId = %Ld" parentId
  )

let event_of_db o =
  Event.({id      = o#!id;       title=o#!title; text = o#!text; datetime = o#!datetime;
          position= o#!position; link = o#?link; timelineId = o#!timelineId })

let event_by_id id : Event.t Lwt.t =
  (*printf "get_event_by_id %Ld\n%!" id;*)
  full_transaction_block (fun dbh ->
    let query = <:select< x | x in $events_table$; x.id = $int64:id$ >> in
    Lwt_Query.query dbh query
      >|= (List.map ~f:event_of_db) >|= view_one
  )

let get_by_pos (pos: int64) timeline_t : Event.t option Lwt.t =
  full_transaction_block (fun dbh ->
    let tlid = Types.id_of_timeline timeline_t in
    let query = <:view< x | x in $events_table$;
                          x.position = $int64:pos$; x.timelineId= $int32:tlid$ >> in
    lwt ans = Lwt_Query.view_opt dbh query in
    Lwt.return @@ Helpers.Option.map ~f:event_of_db ans
  )

let get_by_id = event_by_id

let question_of_db o =
  Question.({id= o#!id;  text=o#!text; options = [] })

let options_id_seq = (<:sequence< bigserial "options_id_seq" >>)
let options_table = (<:table< all_options (
  id              bigint    NOT NULL DEFAULT(nextval $options_id_seq$),
  text            text      NOT NULL,
  questionId      bigint    NOT NULL
) >>)

let questions_for_event eventId =
  full_transaction_block (fun dbh ->
    let q = <:select< x | x in $questions_table$; x.id = $int64:eventId$ >> in
    Lwt_Query.query dbh q
      >|= (List.map ~f:question_of_db)
  )

let option_of_db o = o
let options_for_question questionId =
  full_transaction_block (fun dbh ->
    let q = <:select< x | x in $options_table$; x.id = $int64:questionId$ >> in
    Lwt_Query.query dbh q >|= (List.map ~f:option_of_db)
  )


let make_q_with_options dbh q : Types.Question.t Lwt.t =
  let query = <:select< { id = x.id; qid = x.questionId; text=x.text; } | x in $options_table$; >> in
  lwt opts = Lwt_Query.query dbh query >|= (List.map ~f:(fun o -> (o#!id, o#!qid, o#!text)) ) in
  let options =
    List.filter_map opts ~f:(fun (id,qid,text) -> if qid=q#!id then Some (id,text) else None)
  in

  Lwt.return Question.({id = q#!id; text = q#!text; options })


let questions_id_seq = (<:sequence< bigserial "questions_id_seq" >>)
let questions_table = (<:table< questions (
  id              bigint    NOT NULL DEFAULT(nextval $questions_id_seq$),
  text            text      NOT NULL,
  eventId         bigint    NOT NULL
) >>)


let questions_with_options_of_nodeId eventId =
  lwt qs = full_transaction_block (fun dbh ->
    let q = <:select< {id = x.id; eventId = x.eventId; text=x.text } |
		      x in $questions_table$; x.eventId = $int64:eventId$ >> in
    Lwt_Query.query dbh q
  ) in
  try
    Lwt_list.map_p (fun q -> full_transaction_block (fun dbh -> make_q_with_options dbh q)) qs
  with exn ->
    Exn.to_string exn |> print_endline;
    Lwt.return []

let insert_question ~nodeId ~text =
  let check_q = (<:view< { id=node.id } | node in $events_table$; node.id = $int64:nodeId$; >>) in
  lwt ans = full_transaction_block (fun dbh -> Lwt_Query.view_opt dbh check_q) in
  match ans with
  | None -> Lwt.return false
  | Some x -> begin
      let q = <:insert< $questions_table$ :=
          { id = nextval $questions_id_seq$; text = $string:text$; eventId = $int64:nodeId$; } >>
      in
      lwt () = full_transaction_block (fun dbh -> Lwt_Query.query dbh q) in
      Lwt.return true
    end

let remove_question ~questionId =
  let q = <:delete< q in $questions_table$ | q.id = $int64:questionId$ >> in
  lwt () = full_transaction_block (fun dbh -> Lwt_Query.query dbh q) in
  Lwt.return true

let options_id_seq = (<:sequence< bigserial "all_options_id_seq" >>)
let options_table = (<:table< all_options (
  id                  bigint    NOT NULL DEFAULT(nextval $options_id_seq$),
  text                text      NOT NULL,
  questionId          bigint    NOT NULL
) >>)

let insert_option ~questionId ~text =
  let check_q = <:view< { id=q.id } | q in $questions_table$; q.id = $int64:questionId$; >> in
  lwt ans = full_transaction_block (fun dbh -> Lwt_Query.view_opt dbh check_q) in
  match ans with
  | None -> Lwt.return false
  | Some x -> begin
      lwt () = full_transaction_block (fun dbh -> Lwt_Query.query dbh <:insert< $options_table$:=
          { id = nextval $options_id_seq$; text = $string:text$; questionId = $int64:questionId$; }
      >>) in
      Lwt.return true
    end

let remove_option ~optionId =
  let st = <:delete< opt in $options_table$ | opt.id = $int64:optionId$ >> in
  lwt () = full_transaction_block (fun dbh -> Lwt_Query.query dbh st) in
  Lwt.return true


let event_chain ?(n=3) startId =
  let rec loop dbh n acc nextPos =
    if n=0 then Lwt.return acc else begin
    let query = <:view< x | x in $events_table$;
                        x.timelineId= $int32:101l$; x.position= $int64:nextPos$ >> in
    lwt e = Lwt_Query.view_opt dbh query  in
    match e with
    | None -> Lwt.return acc
    | Some e ->
       let e = event_of_db e in
       let nextPos = Int64.(add 1L e.Event.position) in
       loop dbh (n-1) (e::acc) nextPos
    end
  in
  full_transaction_block (fun dbh ->
    lwt startEvent = get_by_id startId in
    lwt xs = loop dbh n [startEvent] Int64.(add 1L startEvent.Event.position) in
    Lwt.return @@ List.rev xs
  )


let by_date tlsort ~yy ~dd ~mm =
  (* FIXME: don't use +1 for day *)
  let d1 = sprintf "%d-%d-%d" yy mm dd in
  (*printf "Events_db.by_date '%s'\n%!" d1;*)
  let d2 = sprintf "%d-%d-%d" yy mm (dd+1) in
  let timelineId = id_of_timeline tlsort in
  let query = sprintf
    "SELECT id,text,title,link,datetime,position
     FROM events WHERE timelineId=%ld AND datetime between '%s' and '%s' ORDER BY position"
    timelineId d1 d2
  in
  full_transaction_block (fun dbh ->
  lwt () = PGOCaml.prepare dbh ~query () in
  lwt (xs: PGOCaml.row list) = PGOCaml.execute dbh ~params:[] () in
  Lwt.return @@ List.filter_map xs ~f:(function
    | (Some _id)::(Some text)::(Some title)::(link)::(Some _cal)::(Some _pos)::_ ->
      let id = Int64.of_string _id in
      let position = Int64.of_string _pos in
      let datetime = Types.calendar_of_dbstring _cal in
      (*printf "ID=%Ld, title='%s'\n%!" id title;*)
      Some Event.({ id; position; title; text; link; datetime; timelineId })
    | _  -> failwith @@ sprintf "Internal error while getting events by date %d/%d/%d" dd mm yy
  )
  )

let remove_and_get_prev id =
  let query = sprintf "SELECT UkraineRemoveEventById(%Ld)" id in
  (*print_endline query;*)
  full_transaction_block (fun dbh ->
    lwt () = PGOCaml.prepare dbh ~query () in
    lwt (xs: PGOCaml.row list) = PGOCaml.execute dbh ~params:[] () in
    match List.hd_exn xs with
    | (Some _id)::_ -> Lwt.return (Int64.of_string _id)
    | _ -> failwith @@ sprintf "Can't get previous event id when removing event %Ld" id
  )

let remove id =
  lwt _ =  remove_and_get_prev id in
  Lwt.return ()

let find_position_by_datetime_2 tlsort dt =
  let timelineId = id_of_timeline tlsort in
  let query = sprintf "SELECT * FROM GeneralGetPrevEvent2(%ld, '%s')" timelineId
      (dt |> calendar_of_string |> dbstring_of_calendar)
  in
  print_endline query;
  let f = function
    | (Some _id)::(Some title)::(Some text)::(Some _datetime)::(Some _pos)::link::_ ->
      let id = Int64.of_string _id in
      let datetime = Types.calendar_of_dbstring _datetime in
      let position = Int64.of_string _pos in
      Types.Event.({id; datetime; position; text; title; link; timelineId })
    | xs ->
      List.to_string xs ~f:(function Some x -> "'"^x^"'" | None  -> "NULL") |> print_endline;
      print_endline "===========";
      failwith "Error in find_position_by_datetime_2"
  in

  full_transaction_block (fun dbh ->
    lwt () = PGOCaml.prepare dbh ~query () in
    lwt (xs: PGOCaml.row list) = PGOCaml.execute dbh ~params:[] () in
    match xs with
    | x::y::[] -> Lwt.return (Some (f x), Some (f y))
    | x::[]    -> Lwt.return (Some (f x), None)
    | _ -> failwith @@ sprintf "Can't find position by datetime = '%s'" dt
  )

let get_last_event_id tlsort =
  full_transaction_block (fun dbh ->
    let tlid = Types.id_of_timeline tlsort in
    let query = <:view< x order by x.position desc limit 1
                          | x in $events_table$; x.timelineId = $int32:tlid$ >> in
    Lwt_Query.view_one ~log:stdout dbh query >|= (fun x -> x#!id)
  )
