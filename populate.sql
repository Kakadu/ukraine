DROP TABLE IF EXISTS events;

CREATE TABLE events (
  id		SERIAL PRIMARY KEY NOT NULL,
  timelineId    integer,
  position      bigint,
  title		text      NOT NULL,
  datetime 	timestamp NOT NULL,
  text		text,
  link		text
);


DROP TABLE IF EXISTS all_options;
DROP TABLE IF EXISTS questions;

CREATE TABLE questions (
  id   	     SERIAL    PRIMARY KEY NOT NULL,
  eventId    bigint    NOT NULL,
  text	     text      NOT NULL
);
CREATE TABLE all_options (
  id   	     SERIAL    PRIMARY KEY NOT NULL,
  questionId bigint    NOT NULL,
  text	     text      NOT NULL
);

DROP TABLE IF EXISTS comments;

CREATE TABLE comments (
  id        SERIAL PRIMARY KEY NOT NULL /* AUTO INCREMENT */,
  /* собственно путь в дереве комментов */
  path      integer[],  /*NOT NULL*/
  nodeId    integer NOT NULL,  /* which node in tree this comment belongs */
  text      text,
  author    integer

);

DELETE FROM events;
/* dates are in western format: YYYY-MM-DD HH:MM:SS */

CREATE OR REPLACE FUNCTION GeneralInsertAfterPos
(parentPosition bigint, title text, text text, link text, datetime timestamp)
RETURNS void AS $$
DECLARE pos bigint;
/* inserts after parents position. Parents position is selected by parent's id */
BEGIN
  SELECT position INTO pos FROM events where events.position=parentPosition AND timelineId=101;
  IF NOT FOUND THEN
    RAISE EXCEPTION 'parent position % not found', parentPosition;
  END IF;
  UPDATE events SET position=position+1 WHERE position>parentPosition;
  INSERT INTO events (timelineId, title,text, link, datetime, position)
              VALUES (       101, title,text, link, datetime, 1+parentPosition);
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION UkraineInsertAfterPos
(timelineId integer, parentPosition bigint, title text, text text, link text, datetime timestamp)
RETURNS void AS $$
BEGIN
       SELECT GeneralInsertAfterPos(101,parentPosition,title,text,link,datetime);
       RETURN;
END;
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION GeneralInsertAfterParentId
(parentId bigint, title text, text text, link text, datetime timestamp)
RETURNS void AS $$
DECLARE pos bigint;
DECLARE _tlid integer;
/* inserts after parents position. Parents position is selected by parent's id */
BEGIN
  SELECT INTO pos,_tlid
              position,timelineId
  FROM events WHERE events.id=parentId;
  IF NOT FOUND THEN
    RAISE EXCEPTION 'parent position for id % not found', parentId;
  END IF;
  UPDATE events SET position=position+1 WHERE position>pos;
  INSERT INTO events (timelineId, title,text, link, datetime, position)
              VALUES (     _tlid, title,text, link, datetime, 1+pos);
END; $$ LANGUAGE plpgsql;
/*
CREATE OR REPLACE FUNCTION UkraineInsertAfterParentId
(parentId bigint, title text, text text, link text, datetime timestamp)
RETURNS void AS $$
BEGIN
        PERFORM GeneralInsertAfterParentId(101, parentId, title, text, link, datetime);
        RETURN;
END; $$ LANGUAGE plpgsql;
*/
CREATE OR REPLACE FUNCTION UkraineRemoveEventById (_id bigint)
RETURNS bigint AS $$
DECLARE pos bigint;
DECLARE ans bigint;
BEGIN
  SELECT position INTO pos FROM events WHERE events.id=_id;
  IF pos=1 then
    RAISE EXCEPTION 'can not remove root event';
  END IF;

  DELETE FROM events    WHERE id= _id AND timelineId=101;
  DELETE from questions WHERE eventId=_id;
  /* remove options and comments too */
  SELECT id INTO ans FROM events WHERE position=pos-1 AND timelineId=101;
  RETURN ans;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION GeneralGetPrevEvent (_tlid integer, _dt timestamp)
RETURNS bigint AS $$
DECLARE ans bigint;
BEGIN
  SELECT id INTO ans FROM events
  WHERE _dt > events.datetime AND events.timelineId=_tlid
  ORDER BY abs(extract(epoch FROM _dt - events.datetime))
  LIMIT 1;
  RETURN ans;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION UkraineGetPrevEvent (_dt timestamp)
RETURNS bigint AS $$
DECLARE ans bigint;
BEGIN
  SELECT GeneralGetPrevEvent(101, _td) INTO ans;
  RETURN ans;
END $$ LANGUAGE plpgsql;


/* The same but returns  <=2 elements: previous event and next event*/
DROP FUNCTION IF EXISTS GeneralGetPrevEvent2(_tlid integer, timestamp without time zone);

CREATE OR REPLACE FUNCTION GeneralGetPrevEvent2 (_tlid integer, _dt timestamp)
RETURNS
TABLE (id integer, title text, text text, datetime timestamp, pos bigint, link text)
AS $$
DECLARE pos bigint;
BEGIN
  SELECT position INTO pos FROM events
  WHERE _dt > events.datetime AND events.timelineId = _tlid
  ORDER BY abs(extract(epoch FROM _dt - events.datetime))
  LIMIT 1;

  RETURN QUERY
  SELECT events.id,events.title,events.text,events.datetime,events.position,events.link
  AS pos FROM events
  WHERE timelineId=_tlid AND events.position >= pos AND events.position <= pos+1
  ORDER BY position;
END
$$ LANGUAGE plpgsql;

CREATE OR REPLACE FUNCTION UkraineGetPrevEvent2 (_dt timestamp)
RETURNS
TABLE (id integer, title text, text text, datetime timestamp, pos bigint, link text)
AS $$
BEGIN
   RETURN QUERY
   SELECT * FROM GeneralGetPrevEvent2(101, _dt);
   /* ? */
END $$ LANGUAGE plpgsql;