open Eliom_openid
open Lwt

let messages =
  let scope = (Eliom_common.default_process_scope :>  Eliom_common.user_scope) in
  Eliom_state.create_volatile_table ~scope ()

(* The login form *)
let login_form = Eliom_service.App.service
  ~path:["login-form"]
  ~get_params: Eliom_parameter.unit
  ()

(* Initialize the library, and getting the authenticate function *)
let authenticate = Eliom_openid.init ~path:["__openid_return_service"]
    ~f: (fun _ _ -> Eliom_registration.Redirection.send login_form)

(* Create the handler for the form *)
(* We have to use Eliom_registration.String_redirection as we
   redirect the user to her provider *)
let form_handler = Eliom_registration.String_redirection.register_post_coservice
    ~fallback:login_form
    ~post_params: (Eliom_parameter.string "url")
    (fun _ url ->
       authenticate
         ~max_auth_age: 4 (* Requires that if the user logged in more that 4 seconds ago
                             he needs to relog in *)
         ~required: [Eliom_openid.Email] (* Requires his e-mail *)
         ~immediate: false
         url
         (fun result ->
            let string =
              match result with
              | Setup_needed -> "setup needed"
              | Canceled -> "canceled"
              | Result result ->
                try List.assoc Email result.fields with Not_found -> "No e-mail :("
            in
            print_endline string;
            Eliom_state.set_volatile_data ~table:messages string;
            Eliom_registration.Redirection.send login_form
         )
    )

open Eliom_content.Html5.F

module App = Eliom_registration.App(struct
    let application_name = "test"
  end)

let _ =
  let handler _ _ =
    lwt message = match Eliom_state.get_volatile_data ~table: messages () with
      | Eliom_state.Data s ->
        Eliom_state.discard ~scope:Eliom_common.default_process_scope () >>= fun () ->
        Lwt.return [p [pcdata ("Authentication result: "^ s)]]
      | _ -> Lwt.return []
    in
    let form =
      post_form ~service:form_handler
        (fun url ->
           [p [pcdata "Your OpenID identifier: ";
               string_input ~input_type:`Text ~name:url
                 ~value:"https://www.google.com/accounts/o8/id" ();
               string_input ~input_type:`Submit ~value:"Login" ();
              ]]) ()
    in
    let content = message @ [form] in

    Lwt.return
      (html
         (Eliom_tools.F.head ~title:"Test OpenID" ())
         (body content))
  in
  App.register ~service:login_form handler

