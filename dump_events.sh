#!/usr/bin/env bash
export PGUSER=lerss
# This scripts dumps event descriptions. i.e. only data from table `events`
if [ `uname -m` == "kakadu-ocaml-72757" ]; then
  export PGUSER=action
fi
export PGHOST=localhost PGDATABASE=ukraine PGPORT=5432 PGPASSWORD=123
set -x
pg_dump -a --table events > dump_events.sql
pg_dump -a --table questions --table all_options > dump_qandopts.sql


