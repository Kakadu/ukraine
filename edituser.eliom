(* Editing user as moderator
 * toggle switch grabbed from here: http://ghinda.net/css-toggle-switch/
 *)
{shared{
open Eliom_content.Html5
open Eliom_content.Html5.F
open Types
open Helpers
open Printf
open ClientHelpers

let user_editinfo_div_id = "user_editinfo_div"
}}

let dummy_edit_user_service =
  Eliom_service.Http.post_coservice'
    ~name:"set_data"
    ~post_params:Eliom_parameter.(int64 "dummyid" ** (string "dummyfn" ** string "dummyln") )
    ()

let get_user_rpc: (userId_t, Ukraine_user.t) Eliom_pervasives.server_function
  = server_function Json.t<userId_t> (fun userId -> Ukraine_user.user_of_uid userId)

let update_user:  Ukraine_user.t -> unit Lwt.t =
  fun {Ukraine_user.fn; Ukraine_user.ln; Ukraine_user.uid; _} ->
  let open Ukraine_user in
  (* update fn and ln *)
  printf "Update user: %s and %s\n%!" ln fn;
  lwt () = Ukraine_user.update ~firstname:fn ~lastname:ln uid in
  Lwt.return ()

let update_user_rpc: (Ukraine_user.t,unit) Eliom_pervasives.server_function
  = server_function Json.t<Ukraine_user.t> update_user

let toggle_moderator_group ~enable ~userId =
  if enable then Ukraine_groups.set_moderation ~userId
  else Ukraine_groups.unset_moderation ~userId

let toggle_moderator_group_rpc: (userId_t*bool,unit) Eliom_pervasives.server_function
  = server_function Json.t<userId_t*bool>
    (fun (userId,enable) -> toggle_moderator_group ~userId ~enable)

let is_moderator_rpc: (userId_t,bool) Eliom_pervasives.server_function
  = server_function Json.t<userId_t> (fun userId -> Ukraine_groups.is_moderator ~userId)

{client{
open Helpers
let refresh_userinfo ~with_groups userId =
  Firebug.console##log (Js.string "refresh_user_data");
  lwt userinfo     = %get_user_rpc userId in
  lwt is_moderator = %is_moderator_rpc userId in
  Firebug.console##log (Js.string @@ sprintf "user.fn = %s" userinfo.Ukraine_user.fn);
  with_element_by_id_exn user_editinfo_div_id ~ok:(fun el ->
    let form = post_form ~a:[a_class ["user_editinfo_form"]] ~service: %dummy_edit_user_service
	 (fun (id,(fnname,lnname)) ->
      let open Eliom_content.Html5.D in
      let input_fn = string_input ~a:[a_class["user_editinfo_lastname"]] ~name:fnname ~input_type:`Text
	  ~value: userinfo.Ukraine_user.fn () in
      let input_ln = string_input ~a:[a_class["user_editinfo_lastname"]] ~name:lnname ~input_type:`Text
	  ~value: userinfo.Ukraine_user.ln () in
      let apply_clicked _ =
	let fn = (To_dom.of_input input_fn)##value |> Js.to_string in
	let ln = (To_dom.of_input input_ln)##value |> Js.to_string in
        Firebug.console##log (Js.string @@ sprintf "update clicked for %s ans %s" fn ln);
        %update_user_rpc Ukraine_user.({uid=userId; fn; ln; avatar=None}) |> Lwt.ignore_result;
      in

      let apply_btn = div ~a:[ a_class ["button"; "user_editinfo_apply_btn"]
			     ; a_onclick apply_clicked ]         [pcdata "Apply"] in
      [ label ~a:[a_for fnname] [pcdata "First name"]; input_fn; br ()
      ; label ~a:[a_for lnname] [pcdata "Last name"];  input_ln; br ()
      ; apply_btn
      ]
    ) () in
    let roles_switch = match with_groups with
      | true ->
	let set_user_role_clicked _ =
          Firebug.console##log (Js.string "set role = user");
	  Lwt.ignore_result (%toggle_moderator_group_rpc (userId,false))
        in
	let set_moder_role_clicked _ =
          Firebug.console##log (Js.string "set role = moder");
          Lwt.ignore_result (%toggle_moderator_group_rpc (userId,true))
	in
        div ~a:[a_class ["user_editinfo_roles_div"]] [
          div ~a:[a_class ["switch-toggle";"switch-candy"]]
          [ input ~a:([a_id "user_role"; a_name "view"] @
		      (if not is_moderator then [a_checked `Checked] else []))
              ~input_type:`Radio ()
          ; Unsafe.node "label" ~a:[ Unsafe.string_attrib "for" "user_role"
				   ; a_onclick set_user_role_clicked] [pcdata "User"]

          ; input ~a:([a_id "moder_role"; a_name "view"] @
                      (if is_moderator then [a_checked `Checked] else []))
              ~input_type:`Radio ()
          ; Unsafe.node "label" ~a:[ Unsafe.string_attrib "for" "moder_role"
				   ; a_onclick set_moder_role_clicked] [pcdata "Moderator"]
          ; Unsafe.node "a" []
          ]
        ]
      | false -> div []
    in
    (* Some shit here: form and roles div don't want to be stacked horizontally *)
    Manip.replaceChildren (el |> Of_dom.of_element) [div [roles_switch;br();form]]
  ) ~bad:(fun id -> Firebug.console##warn (Js.string @@ sprintf "can't get element by id '%s'" id));

  Lwt.return ()

}}

let view_handler (moderatorId: userId_t) (userId: userId_t) () =
  lwt user = Ukraine_user.user_of_uid userId in

  let refresh_userinfo_btn_clicked = { Dom_html.mouseEvent Js.t -> unit { fun _event ->
      Dom.preventDefault _event;
      Lwt.ignore_result @@ refresh_userinfo ~with_groups:true %userId
  }} in
  let onload_userinfo_div = { Dom_html.event Js.t -> unit { fun _ ->
      Lwt.ignore_result @@ refresh_userinfo ~with_groups:true %userId;
  }} in

  let content_div =
    div ~a:[a_class ["user_editinfo_div_parent"]]
      [ div ~a:[a_class ["user_editinfo_refresh_btn"]; a_onclick refresh_userinfo_btn_clicked]
          [ dummy_img() ]
      ; br ()
      ; div ~a:[a_id user_editinfo_div_id; a_onload onload_userinfo_div] []
      ; br ()
      ]
  in

  Lwt.return (Ukraine_container.page ~user:moderatorId [
    content_div
  ])

let profile_handler (userId: userId_t) () () =
  let refresh_userinfo_btn_clicked = { Dom_html.mouseEvent Js.t -> unit { fun _event ->
      Dom.preventDefault _event;
      Lwt.ignore_result @@ refresh_userinfo ~with_groups:false %userId
  }} in
  let onload_userinfo_div = { Dom_html.event Js.t -> unit { fun _ ->
      Lwt.ignore_result @@ refresh_userinfo ~with_groups:false %userId;
  }} in

  let content_div =
    div ~a:[a_class ["user_editinfo_div_parent"]]
      [ div ~a:[a_class ["user_editinfo_refresh_btn"]; a_onclick refresh_userinfo_btn_clicked]
          [ dummy_img() ]
      ; br ()
      ; div ~a:[a_id user_editinfo_div_id; a_onload onload_userinfo_div] []
      ; br ()
      ]
  in

  Lwt.return @@ Ukraine_container.page ~user:userId [ content_div ]

(* TODO: we should check that moderator can't edit its own profile there *)
let () = Ebapp.App.register Ukraine_services.edit_user_service
    (Ebapp.Page.connected_page ~fallback:LoginError.page  view_handler)

let () = Ebapp.App.register Ukraine_services.edit_profile_service
    (Ebapp.Page.connected_page ~fallback:LoginError.page profile_handler)
