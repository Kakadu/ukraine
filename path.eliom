{shared{
open Helpers
open Printf

type 'a tree = Node of 'a * 'a tree list

type t = int64 list deriving (Json)

let length = List.length
let length_of_node = function Node(x,_) -> length x

let to_string t =
  let (_:int64 -> string) = Int64.to_string in
  let (_: f:('a -> 'b) -> 'a list -> 'b list) = Helpers.List.map in
  let boo = List.map t ~f:Int64.to_string in
  boo  |> String.concat ~sep:","

let parentId path = match List.rev path with
  | _::x::_ -> x
  | _ -> failwith "bad argument of Path.parentId"

(* converts lines "{1,2,3}" to [1;2;3] *)
let of_string_exn s =
  String.sub s 1 (String.length s-2) |> String.split ~on:',' |> List.map ~f:Int64.of_string

let make_child t id = t@[id]

let unfoldr: f:('a list -> ('b list * 'a list)) -> 'a list -> ('b list * 'a list) = fun  ~f xs ->
  let rec helper acc xs =
    match f xs with
    | ([],tl) -> (acc,tl)
    | (ys,tl) -> (ys@acc,tl)
  in
  let (a,b) = helper [] xs in
  (List.rev a, b)

let make_tree (xs: t list) =
  let rec helper len xs =
    (*printf "helper %d %s\n%!" len (List.to_string xs ~f:Path.to_string);*)
    match xs with
    | [] -> ([],[])
    | x::xs when length x = len ->
       let (ans,tl) = unfoldr xs ~f:(helper (len+1)) in
       let anspart = if List.length ans = 0 then Node (x,[])  else Node (x,List.rev ans) in
       let (ans2, tl2) = helper len tl in
       (anspart::ans2, tl2)
    | x::xs when length x > len -> assert false
    | y::ys when length y < len -> ([],xs)
    | ___  -> ([],[])
  in
  helper 1 xs


let draw_tree ts =
  let rec helper padding = function
    | Node (v,xs) ->
       let pad = String.make (padding*2) ' ' in
       printf "%s%s\n" pad (to_string v);
       List.iter xs ~f:(helper (padding+1))
  in
  List.iter ts ~f:(helper 0)

(* The same as make_tree bu got a list of < path: t; .. > *)
let make_tree_of_nodes path xs =
  let rec helper len xs =
    (*printf "helper %d %s\n%!" len (List.to_string xs ~f:Path.to_string);*)
    match xs with
    | [] -> ([],[])
    | x::xs when length @@ path x = len ->
       let (ans,tl) = unfoldr xs ~f:(helper (len+1)) in
       let anspart = if List.length ans = 0 then Node (x,[])  else Node (x,List.rev ans) in
       let (ans2, tl2) = helper len tl in
       (anspart::ans2, tl2)
    | x::xs when length @@ path x > len -> assert false
    | y::ys when length @@ path y < len -> ([],xs)
    | ___  -> ([],[])
  in
  helper 1 xs

}}
