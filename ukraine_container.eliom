{shared{
  open Eliom_content.Html5
  open Eliom_content.Html5.F
  open Ukraine_tools
  open Printf
}}

let header ?user () =
  let navbar_cls =
    match user with
      | None -> ["absolute"; "right"]
      | Some _ -> ["inline-block"]
  in
  let user_box =
    match user with
      | None -> nothing ()
      | Some _ ->
          div ~a:[a_id "ukraine-user-box"; a_class ["absolute"; "right"; "bottom"; "inline-block"]]
	    [ Ukraine_view.profile_button ()
            ; Ukraine_view.disconnect_button ()
            ]
  in
  div ~a:[a_id "ukraine-header"; a_class ["center"]] [
    a ~a:[a_id "ukraine-logo"]
      ~service:Ukraine_services.main_service [ pcdata Ebapp.App.app_name ] ();
    div ~a:[a_id "ukraine-navbar"; a_class ("bottom"::navbar_cls)]
      [ a ~a:[a_class ["item"; "left-bar"]] ~service:Ukraine_services.main_service
        [pcdata (match user with None -> "Login" | _ -> "Today")] ()
      ; a ~a:[a_class ["item"; "left-bar"]]
          ~service:Ukraine_services.ukraine_last_service [pcdata "Ukraine last event"] ()
      ; a ~a:[a_class ["item"; "left-bar"]]
          ~service:Ukraine_services.grf_last_service [pcdata "GRF last event"] ()
      ; a ~a:[a_class ["item"; "left-bar"]]
        ~service:Ukraine_services.about_service [pcdata "About"] ()
    ];
    user_box;
  ]

let footer ?user () =
  div ~a:[a_id "ukraine-footer"; a_class ["center"]] [
    span ~a:[a_class ["eba-template"]] [
      pcdata "This application has been generated using ";
      a ~service:Ukraine_services.eba_github_service [
        pcdata "Eliom-base-app"
      ] ();
      pcdata " template and uses ";
      a ~service:Ukraine_services.ocsigen_service [
        pcdata "Ocsigen project"
      ] ();
      pcdata " technology.";
    ];
  ]

let page ?user cnt =
  [
    header ?user ();
    div ~a:[a_id "ukraine-body"; a_class ["center"]]
      (div ~a:[a_id "ukraine-request-msgs"]
         ( (List.map (Ebapp.Reqm.to_html) (Ebapp.Reqm.to_list Ukraine_reqm.notice_set))
         @ (List.map (Ebapp.Reqm.to_html) (Ebapp.Reqm.to_list Ukraine_reqm.error_set)))
       ::cnt);
(*    footer ?user ();*)
  ]
