--
-- PostgreSQL database dump
--

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

--
-- Data for Name: all_options; Type: TABLE DATA; Schema: public; Owner: lerss
--

COPY all_options (id, questionid, text) FROM stdin;
1	1	Альфа
2	1	Беркут
3	1	Спецназ РФ
4	1	Спецназ НАТО
5	1	Правый сектор
6	1	Оружие было
7	1	Оружия не было
\.


--
-- Name: all_options_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lerss
--

SELECT pg_catalog.setval('all_options_id_seq', 7, true);


--
-- Data for Name: questions; Type: TABLE DATA; Schema: public; Owner: lerss
--

COPY questions (id, eventid, text) FROM stdin;
1	2	Кто стрелял?
2	2	Было ли у майданутых оружие
3	7	Откуда деньги?
\.


--
-- Name: questions_id_seq; Type: SEQUENCE SET; Schema: public; Owner: lerss
--

SELECT pg_catalog.setval('questions_id_seq', 3, true);


--
-- PostgreSQL database dump complete
--

