(* Database interaction for getting/inserting comments to nodes *)
open Ukraine_db
open Printf
open Types
open Helpers

let object_of_node: PGOCaml.row -> Comment.t = function
  | (Some _id)::(Some path_str)::(Some author)::(Some text) :: _      ->
    Comment.({ id = Int64.of_string _id
             ; path = Path.of_string_exn path_str
             ; text = text
             ; author = Int64.of_string author
            })
  | _ -> failwith "got bad node from database"

let comment_data_for_event eventId : Types.CommentWithUINFO.t list Lwt.t =
  let f = function
    | (Some _cid)::(Some _path)::(Some text)::(Some _uid):: _fn :: _ln ::_ ->
      let open Types.CommentWithUINFO in
      { id = Int64.of_string _cid
      ; path = Path.of_string_exn _path
      ; text
      ; author = Int64.of_string _uid
      ; fn = Option.get ~default:"" _fn
      ; ln = Option.get ~default:"" _ln
      }
    | _ -> failwith "Error in comment_data_from_event"
  in
  full_transaction_block (fun dbh ->
    let query = sprintf "SELECT comments.id,path,text,author,users.firstname,users.lastname
      FROM comments LEFT JOIN users ON author=users.userid
      WHERE comments.nodeid=%Ld ORDER BY path" eventId in
    lwt () = PGOCaml.prepare dbh ~query () in
    lwt (ans: PGOCaml.row list) = PGOCaml.execute dbh ~params:[] () in
    Lwt.return @@ List.map ~f ans
  )

let comments_for_node nodeId : Comment.t list Lwt.t =
  full_transaction_block (fun dbh ->
    let query =
      sprintf "SELECT id,path,author,text FROM comments WHERE nodeId=%Ld ORDER BY path" nodeId in
    lwt () = PGOCaml.prepare dbh ~query () in
    lwt (ans: PGOCaml.row list) = PGOCaml.execute dbh ~params:[] () in
    Lwt.return (List.map ~f:object_of_node ans)
  )

let comment_by_id id =
  full_transaction_block (fun dbh ->
    let query = sprintf "SELECT id,path,author,text FROM comments WHERE id=%Ld" id in
    lwt () = PGOCaml.prepare dbh ~query () in
    lwt ans = PGOCaml.execute dbh ~params:[] () in
    Lwt.return (List.hd_exn ans |> object_of_node)
  )

(*
let comments_id_seq = (<:sequence< bigserial "comments_id_seq" >>)
let comments_table = (<:table< comments (
  id              bigint    NOT NULL DEFAULT(nextval $nodes_id_seq$),
  text            text      NOT NULL,
  link            text      NOT NULL,
  picture         text      NOT NULL,
  title           text      NOT NULL
) >>)
		     *)

let insert_subcomment ~parentId ~text ~nodeId ~userId =
  lwt parent_comment = comment_by_id parentId in
  let nodeId32 = Int64.to_int32 nodeId in
  let userId32 = Int64.to_int32 userId in
  full_transaction_block (fun dbh ->
    lwt () = PGSQL(dbh) "INSERT INTO comments(path,   nodeid, text,   author)
			 VALUES              ('{}',$nodeId32,$text,$userId32)"
    in

    match_lwt PGSQL(dbh) "SELECT currval('comments_id_seq')" with
    | (Some lastId) :: _ ->
      printf "parent comment path = %s\n%!" (Path.to_string parent_comment.Comment.path);
      let s = Path.make_child parent_comment.Comment.path lastId |> Path.to_string in
      let query = sprintf "UPDATE comments SET path='{%s}' WHERE id=%Ld" s lastId in
      (*printf "query = '%s'\n%!" query;*)
      lwt () = PGOCaml.prepare dbh ~query () in
      lwt _ = PGOCaml.execute dbh ~params:[] () in
      Lwt.return ()
    | _  -> failwith "Internal error"
  )

let insert_comment ~text ~nodeId ~userId =
  let nodeId32 = Int64.to_int32 nodeId in
  let userId32 = Int64.to_int32 userId in
  full_transaction_block (fun dbh ->
    lwt () = PGSQL(dbh) "INSERT INTO comments(path,   nodeid, text,   author)
			 VALUES              ('{}',$nodeId32,$text,$userId32)"
    in
    match_lwt PGSQL(dbh) "SELECT currval('comments_id_seq')" with
    | (Some _lastid)::_ ->
      let lastId = Int64.to_int _lastid in
      let query = sprintf "UPDATE comments SET path='{%d}' WHERE id=%d" lastId lastId in
      lwt () = PGOCaml.prepare dbh ~query () in
      lwt _ = PGOCaml.execute dbh ~params:[] () in
      Lwt.return ()
    | _  -> failwith "Internal error"
  )

(*
let node_and_direct_children parentId =
  full_transaction_block (fun dbh ->
    let query = Printf.sprintf
      "SELECT id,path,title,text,link,picture FROM nodes WHERE path && ARRAY[%d]" parentId in
    lwt () = PGOCaml.prepare dbh ~query () in
    lwt (ans: PGOCaml.row list) = PGOCaml.execute dbh ~params:[] () in
    let ans = List.map ~f:object_of_node ans in
    let root = List.find ans ~f:(fun o -> o#id=parentId)
	       |> (function Some o -> o | None -> assert false) in
    let parentPathLen = List.length root#path in
    let children = List.filter ans ~f:(fun o -> List.length o#path = parentPathLen + 1) in
    Lwt.return (root, children)
  )
*)
