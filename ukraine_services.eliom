open Eliom_parameter

let main_service =
  Eliom_service.App.service
    ~path:[]
    ~get_params:unit ()

let forgot_password_service =
  Eliom_service.App.service
    ~path:["forgot-password"]
    ~get_params:unit ()

let preregister_service' =
  Eliom_service.Http.post_coservice'
    ~name:"preregister_service"
    ~post_params:(string "email")
    ()

let about_service =
  Eliom_service.App.service
    ~path:["about"]
    ~get_params:unit ()

let forgot_password_service' =
  Eliom_service.Http.post_coservice'
    ~name:"lost_password"
    ~post_params:(string "email")
    ()

let set_personal_data_service' =
  Eliom_service.Http.post_coservice'
    ~name:"set_data"
    ~post_params:(
      (string "firstname" ** string "lastname")
      ** (string "password" ** string "password2"))
    ()

let sign_up_service' =
  Eliom_service.Http.post_coservice'
    ~name:"sign_up"
    ~post_params:(string "email")
    ()

let connect_service =
  Eliom_service.Http.post_coservice'
    ~name:"connect"
    ~post_params:(string "login" ** string "password") ()

let disconnect_service =
  Eliom_service.Http.post_coservice'
    ~name:"disconnect"
    ~post_params:unit ()

let activation_service =
  Eliom_service.Http.coservice'
    ~name:"activation"
    ~get_params:(string "activationkey") ()

let eba_github_service =
  Eliom_service.Http.external_service
    ~prefix:"http://"
    ~path:["github.com"; "ocsigen"; "eliom-base-app"]
    ~get_params:Eliom_parameter.unit ()

let ocsigen_service =
  Eliom_service.Http.external_service
    ~prefix:"http://"
    ~path:["ocsigen.org"]
    ~get_params:Eliom_parameter.unit ()

(* edit user's profile *)
let edit_user_service =
  Eliom_service.App.service ~path:["edituser"] ~get_params:(suffix (int64 "id")) ()

let edit_profile_service =
  Eliom_service.App.service ~path:["profile"] ~get_params:unit ()


(* Event related services *)

(* view one element of timeline *)
let view_event_service =
  Eliom_service.App.service ~path:["view"] ~get_params:(suffix (int64 "id")) ()

(* edit event on timeline *)
let edit_event_service =
  Eliom_service.App.service ~path:["edit"] ~get_params:(suffix (int64 "id")) ()

(* show events on this date *)
let view_date_service =
  Eliom_service.App.service ~path:["when"]
    ~get_params: (suffix ((int "year") ** (int "month") ** (int "day") ) )
    ()

(* view subtree *)
let tree_view_service =
  Eliom_service.App.service
    ~path:["treeview"]
    ~get_params:(suffix (int64 "id"))
    ()

(* adds event after parent *)
let add_node_service =
  Eliom_service.App.service ~path:["add"] ~get_params:(suffix (int64 "parent")) ()

let smart_add_service =
  Eliom_service.App.service ~path:["smartadd"] ~get_params:(suffix @@ int32 "tlid") ()

(* Last pages for every timeline *)
let ukraine_last_service =
  Eliom_service.App.service ~path:["ukraine-last"] ~get_params:unit ()

let grf_last_service =
  Eliom_service.App.service ~path:["grf-last"] ~get_params:unit ()

