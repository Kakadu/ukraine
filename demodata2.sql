/** In this file demo data located for users and comments.*/

/*
 * default users and moderators
 */
INSERT INTO users(firstname, lastname, password, avatar) VALUES
  ('Moderator','Main','$2y$06$bVN6UpfLhSAdzrBUBmqO1u/GvPsWkg58qgsnQQIQq9/22qXdodnUS','') /* mmm123 */
, ('Test',     'User','$2y$06$zT0bVKhPT6aIBDbZpYv3IO7gFx56vx9GDMfoKClLlkztu7o4QXPJW','') /* uuu123 */
,('Lerss','SkyComposer','$2y$06$KJ/c2EDWtvHGQjWWwwZ6X.mMD8G.K6AF2eJEhp.Hx5rztEinciOri','')/*191821*/
;

INSERT INTO emails(email,userid) VALUES
  ('m@m', 1)      /* default moderator */
, ('u@u', 2)      /* Test User */
, ('skycomposer@gmail.com', 3)      /* Lerss */
;

INSERT INTO groups(name,description) VALUES
  ('moderators', 'Allowed to add new nodes and edit them')
;
INSERT INTO user_groups(userid,groupid) VALUES
  (1, 1)
, (3, 1)
;

/*
 * comments for 1st event
 */
INSERT INTO comments (path, author, nodeId, text) VALUES
 ('{1}',      11, 1,
             'Комментарий 1')
,('{2}',      11, 1,
             'Комментарий 2')
,('{3}',     11, 1,
             'Комментарий 3')
,('{1,4}',   11, 1,
             'Комментарий к 1')
,('{1,4,5}', 11, 1,
             'Комментарий к 1,4')
,('{3,6}',   11, 1,
             'Комментарий 3,6')
,('{3,6,7}', 11, 1,
             'Комментарий 3,6,7')
,('{3,6,8}', 11, 1,
             'Комментарий 3,6,8')
;

