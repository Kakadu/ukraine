open Helpers
open Eliom_content.Html5
open Eliom_content.Html5.F

let page : int64 option -> _ -> _ -> exn -> Ebapp.Page.page_content Lwt.t = fun userId _ _ _exn ->

  Lwt.return
    [ div ~a:[a_class ["login_error_box"] ]
	[ pcdata @@ "Login error "
        ; pcdata @@ Exn.to_string _exn
        ; br ()
	; div [pcdata (match userId with Some x -> Int64.to_string x | None -> "None")]
        ; br ()
        ; div [pcdata @@ Exn.backtrace ()]
        ]
    ]
