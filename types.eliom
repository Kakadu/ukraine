{shared{
open Helpers
open Eliom_content.Html5
open Eliom_content.Html5.F

type userId_t    = int64 deriving (Json)
type eventId_t   = int64 deriving (Json)
type commentId_t = int64 deriving (Json)
type optionId_t  = int64 deriving (Json)

type timeline_t = Maidan2013 | GreatRusFirewall deriving (Json)
let id_of_timeline = function
  | Maidan2013 -> 101l
  | GreatRusFirewall -> 22l

let timeline_of_id = function
  | 101l -> Maidan2013
  | 22l  -> GreatRusFirewall
  | _ -> raise (Failure "Bad argument of timepline_of_id")

let timeline_start_event = function
  | Maidan2013 -> 101L
  | GreatRusFirewall -> 22L

type calendar = CalendarLib.Calendar.t
let _calendar_format    = "%d-%m-%Y %H:%M:%S"
let _calendar_format_db = "%Y-%m-%d %H:%M:%S"
let string_of_calendar   = CalendarLib.Printer.Calendar.sprint _calendar_format
let dbstring_of_calendar = CalendarLib.Printer.Calendar.sprint _calendar_format_db
let string_of_date       = CalendarLib.Printer.Calendar.sprint "%d-%m-%Y"
let string_of_time       = CalendarLib.Printer.Calendar.sprint "%H:%M:%S"
let calendar_of_string   = CalendarLib.Printer.Calendar.from_fstring _calendar_format
let calendar_of_dbstring = CalendarLib.Printer.Calendar.from_fstring _calendar_format_db

module Json_calendar : Deriving_Json.Json_min with type a = calendar = struct
  type a = calendar
  let write b x : unit = Buffer.add_string b (string_of_calendar x)
  let read lexbuf = Deriving_Json_lexer.read_string lexbuf |> calendar_of_string
end
module Event = struct
  type t =
    { id   : int64
    ; title: string
    ; text : string
    ; link : string option
    ; position: int64
    ; timelineId : int32
    ; datetime: calendar
    } deriving (Json)
end

module Comment = struct
 type t =
   { id: int64
   ; path: Path.t
   ; text: string
   ; author: int64
   } deriving (Json)

end

module CommentWithUINFO = struct
 type t =
   { id: int64
   ; path: Path.t
   ; text: string
   ; author: int64
   ; fn: string
   ; ln: string
   } deriving (Json)
end

(* TODO: rename Option to interpretation *)
module Option = struct
 type t = int64 * string deriving (Json)
end

type questionId_t = int64 deriving (Json)

module Question = struct
 type t =
   { id  : int64
   ; text: string
   ; options: Option.t list
   } deriving (Json)
end

}}
