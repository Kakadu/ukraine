(* Service handler for adding new events.
 * TODO: refactor code to allow editing of events
 *)
open Printf

{shared{
open Types
open Eliom_content.Html5
open Eliom_content.Html5.F
type datetime_str = string deriving (Json)
let add_event_date_input_id = "add_event_date_input"
let add_event_time_input_id = "add_event_time_input"
}}

{client{
open Printf
open Helpers
open ClientHelpers
open Ojquery
}}

let dummy_service' =
  Eliom_service.Http.post_coservice  ~fallback:Ukraine_services.view_event_service
    ~post_params:Eliom_parameter.(string "title"**string "image" ** string "lenta_text" ** string "lenta_url")
    ()

let insert_event_rpc
  : (eventId_t*string*string*string*datetime_str,eventId_t) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t*string*string*string*datetime_str>
    (fun (parentId,title,text,link,datetime_str) ->
       Events_db.insert_after ~text ~link ~title ~datetime_str ~parentId)

let add_node_handler userId (eventId: int64) () : _ Lwt.t =
  lwt parent_event = Events_db.get_by_id eventId in
  let tlsort = timeline_of_id parent_event.Event.timelineId in
  lwt next_event = Events_db.get_by_pos Int64.(add 1L parent_event.Event.position) tlsort in

  let open Eliom_content.Html5.D in
  let form = post_form ~service:dummy_service' (fun (title,(image,(lenta_text,lenta_url))) ->
    let title_input = string_input ~a:[a_placeholder "title"; a_class ["add_node_line_input"]]
        ~name:title ~input_type:`Text () in
    let lenta_input = string_input ~a:[a_placeholder "url"; a_class ["add_node_line_input"]]
        ~name:lenta_url ~input_type:`Url () in
    let text_input = textarea ~a:[a_placeholder "text"; a_class ["add_node_line_input"]]
        ~name:lenta_text ()
    in
    let date_input = string_input ~name:lenta_text ~input_type:`Text
        ~value:(string_of_date parent_event.Event.datetime) ()
        ~a:[ a_placeholder "date"; a_class ["datepicker";"add_node_date_input"]
           ; a_id add_event_date_input_id ]
    in
    let time_input = string_input ~name:lenta_text ~input_type:`Text
        ~value:(string_of_time parent_event.Event.datetime) ()
        ~a:[ a_placeholder "time"; a_class ["timepicker";"add_node_time_input"]
           ; a_id add_event_time_input_id ]
    in
    let submit_clicked = {Dom_html.mouseEvent Js.t -> unit { fun _event ->
      Dom.preventDefault _event;
      (* TODO: we need to check that date is >parentDate and  < nextDate *)
      let title    = (To_dom.of_input %title_input)##value |>  Js.to_string in
      let text     = (To_dom.of_textarea %text_input)##value |> Js.to_string in
      let link     = (To_dom.of_input %lenta_input)##value |> Js.to_string in
      let datetime = sprintf "%s %s"
         ( (To_dom.of_input %date_input)##value |> Js.to_string )
         ( (To_dom.of_input %time_input)##value |> Js.to_string )
      in
      Lwt.ignore_result begin
      lwt newId = %insert_event_rpc (%eventId, title, text, link, datetime) in
      Eliom_client.exit_to ~service: %Ukraine_services.view_event_service newId ();
      Lwt.return ()
      end
    }} in

    [ title_input; br ()
    ; lenta_input; br ()
    ; pcdata "Date"; date_input; pcdata "Time";  time_input; br ()
    ; text_input ; br ()
    ; string_input ~a:[a_class ["button"]; a_onclick submit_clicked ]
        ~input_type:`Button ~value:"Add new node" ()
    ]
    ) userId
  in
  let content_div_onload = {Dom_html.event Js.t -> unit { fun _event ->
    (* I don't do this in input.onload event because by some reason is didn't work for me *)
    (* TODO: understand how to do it typesafe *)
    let el = get_element_by_id_exn add_event_date_input_id in
    let date_params: datepicker_options Js.t = Js.Unsafe.obj [||] in
    (* International english format *)
    date_params##dateFormat <- Js.string "dd-mm-yy";
    el |> jQelt |> (jq_datepicker date_params) |> ignore;

    let el = get_element_by_id_exn add_event_time_input_id in
    let time_params: timepicker_options Js.t =
      Js.Unsafe.eval_string "new Object({step: 5, timeFormat: 'H:i:s'})" in
    el |> jQelt |> (jq_timepicker time_params) |> ignore;
  }} in

  let make_event_desc_div ~msg clas e =
    let xs = match e with
      | Some e ->
        [ pcdata msg; br ()
        ; pcdata @@ sprintf "Id    = %Ld" e.Event.id; br ()
        ; pcdata @@ sprintf "Title = %s" e.Event.title; br ()
        ; pcdata @@ sprintf "When  = %s" (string_of_calendar e.Event.datetime); br ()
        ]
      | None -> []
    in
    div ~a:[a_class clas] xs
  in
  let content_div = div ~a:[a_onload content_div_onload]
    [ make_event_desc_div ~msg:"Previous event will be:"
        ["add_event_parent_desc";"float-left"]  (Some parent_event)
    ; make_event_desc_div ~msg:"Next event will be:"
        ["add_event_parent_desc";  "float-right"] next_event
    ; form
    ]
  in

  Lwt.return (Ukraine_container.page [
    content_div
  ])

let () =
  Ebapp.App.register Ukraine_services.add_node_service
    (Ebapp.Page.connected_page add_node_handler)
