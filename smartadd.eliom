(* Adding new events in smart way: enter the date and it will decide where to place it
 *)
{shared{
open Printf
open Types
open Helpers
open Eliom_content.Html5
open Eliom_content.Html5.F

let add_event_date_input_id = "add_event_date_input"
let add_event_time_input_id = "add_event_time_input"
let smart_add_btn_id = "smart_add_btn"
let smart_add_message_text_id = "smart_add_message_text"
}}

{client{
open ClientHelpers
open Ojquery
open Firebug

let curParent: int64 option ref  = ref None
}}

let dummy_service' =
  Eliom_service.Http.post_coservice  ~fallback:Ukraine_services.view_event_service
    ~post_params:Eliom_parameter.
                   (string "title"**string "image" ** string "lenta_text" ** string "lenta_url")
    ()

let insert_event_rpc
  : (eventId_t*string*string*string*string,eventId_t) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t*string*string*string*string>
    (fun (parentId,title,text,link,datetime_str) ->
       Events_db.insert_after ~text ~link ~title ~datetime_str ~parentId)

let find_position2_rpc
  : (timeline_t*string, Types.Event.t option * Types.Event.t option) Eliom_pervasives.server_function
  = server_function Json.t<timeline_t*string>
    (fun (tlid,s) -> Events_db.find_position_by_datetime_2 tlid s)


let handler userId tlid () : _ Lwt.t =
  let () =
    try ignore (Types.timeline_of_id tlid)
    with _ -> raise (Failure "Bad timeline id")
  in

  let open Eliom_content.Html5.D in
  let form = post_form ~service:dummy_service' (fun (title,(image,(lenta_text,lenta_url))) ->
    let datetime_changed = {Dom_html.event Js.t  -> unit{ fun _ ->
      let date: Dom_html.inputElement Js.t =
        Js.Unsafe.coerce (get_element_by_id_exn %add_event_date_input_id) in
      let time: Dom_html.inputElement Js.t =
        Js.Unsafe.coerce (get_element_by_id_exn %add_event_time_input_id) in
      let text_block = get_element_by_id_exn %smart_add_message_text_id in
      let prev_desc  = get_element_by_id_exn "smart_add_event_prev_desc" in
      let next_desc  = get_element_by_id_exn "smart_add_event_next_desc" in
      let update_desc ~msg el event =
        let xs = match event with
          | Some e ->
            [ pcdata msg; br ()
            ; pcdata @@ sprintf "Id    = %Ld" e.Event.id; br ()
            ; pcdata @@ sprintf "Title = %s" e.Event.title; br ()
            ; pcdata @@ sprintf "When  = %s" (string_of_calendar e.Event.datetime); br ()
            ]
          | None -> []
        in
        Manip.replaceChildren (Of_dom.of_element el) xs
      in
      let open Ojquery in
      (try_lwt
        let s = sprintf "%s %s" (Js.to_string date##value) (Js.to_string time##value) in
        console##log (Js.string s);
        let _ = calendar_of_string s in
        (
         lwt (l,r) = %find_position2_rpc (timeline_of_id %tlid,s) in
         curParent := (match l with Some x -> Some x.Event.id | None -> None);
         update_desc ~msg:"PREV" prev_desc l;
         update_desc ~msg:"NEXT" next_desc r;
         Lwt.return ()
        )
      with
      | exc ->
        console##log_2 (Js.string @@ Exn.to_string exc, Js.string "datetime changed");
        Lwt.return ()
      )  |> Lwt.ignore_result;
      let () = match !curParent with
        | Some id -> text_set (jQelt text_block) "OK" |> ignore
        | None -> text_set (jQelt text_block) "wrong date time" |> ignore
      in
      ()
    }} in
    let date_input = string_input ~name:lenta_text ~input_type:`Text ()
                                  (*~value:("14-03-2014") *)
        ~a:[ a_placeholder "date"; a_class ["datepicker";"add_node_date_input"]
           ; a_id add_event_date_input_id; a_onchange datetime_changed ]
    in
    let time_input = string_input ~name:lenta_text ~input_type:`Text ()
                                  (* ~value:("00:20:00") *)
        ~a:[ a_placeholder "time"; a_class ["timepicker";"add_node_time_input"]
           ; a_id add_event_time_input_id; a_onchange datetime_changed  ]
    in

    let title_input = string_input ~a:[a_placeholder "title"; a_class ["add_node_line_input"]]
        ~name:title ~input_type:`Text () in
    let lenta_input = string_input ~a:[a_placeholder "url"; a_class ["add_node_line_input"]]
        ~name:lenta_url ~input_type:`Url () in
    let text_input = textarea ~a:[a_placeholder "text"; a_class ["smart_add_textarea"]]
        ~name:lenta_text ()
    in
    let submit_clicked = {Dom_html.mouseEvent Js.t -> unit { fun _event ->
      Dom.preventDefault _event;

      let title    = (To_dom.of_input %title_input)##value |>  Js.to_string in
      let text     = (To_dom.of_textarea %text_input)##value |> Js.to_string in
      let link     = (To_dom.of_input %lenta_input)##value |> Js.to_string in
      let datetime = sprintf "%s %s"
         ( (To_dom.of_input %date_input)##value |> Js.to_string )
         ( (To_dom.of_input %time_input)##value |> Js.to_string )
      in
      Lwt.ignore_result begin
      let parentId = Option.get_exn !curParent in
      lwt newId = %insert_event_rpc (parentId, title, text, link, datetime) in
      Eliom_client.exit_to ~service: %Ukraine_services.view_event_service newId ();
      Lwt.return ()
      end
    }} in

    [ pcdata "Date"; date_input; br ()
    ; pcdata "Time"; time_input; br ()
    ; div ~a:[a_id smart_add_message_text_id] [pcdata ""]; br ()
    ; title_input; br ()
    ; lenta_input; br ()
    ; text_input ; br ()
    ; string_input ~a:[a_class ["button"]; a_id smart_add_btn_id; a_onclick submit_clicked ]
        ~input_type:`Button ~value:"Add new node" ()
    ]
    ) userId
  in
  let content_div_onload = {Dom_html.event Js.t -> unit { fun _event ->
    (* I don't do this in input.onload event because by some reason is didn't work for me *)
    (* TODO: understand how to do it typesafe *)
    let el = get_element_by_id_exn add_event_date_input_id in
    let date_params: datepicker_options Js.t = Js.Unsafe.obj [||] in
    date_params##dateFormat <- Js.string "dd-mm-yy";
    el |> jQelt |> (jq_datepicker date_params) |> ignore;

    let el = get_element_by_id_exn add_event_time_input_id in
    let time_params: timepicker_options Js.t =
      Js.Unsafe.eval_string "new Object({step: 5, timeFormat: 'H:i:s'})" in
    el |> jQelt |> (jq_timepicker time_params) |> ignore;
  }} in
  let content_div = div ~a:[a_onload content_div_onload]
    [ div ~a:[a_class  ["float-left"]; a_id "smart_add_event_prev_desc"] []
    ; div ~a:[a_class ["float-right"]; a_id "smart_add_event_next_desc"] []
    ; form
    ]
  in

  Lwt.return (Ukraine_container.page ~user:userId [
    content_div
  ])

let () = Ebapp.App.register Ukraine_services.smart_add_service
    (Ebapp.Page.connected_page ~fallback:LoginError.page handler)
