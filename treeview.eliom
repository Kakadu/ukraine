(* There you can find service which draws in pretty tree-like structure
 * My goal is to create web-page with easy navigation over timeline.
 *)

open Helpers
open Printf
open Types

{shared{
  open Eliom_content.Html5
  open Eliom_content.Html5.F
}}

let tree_view_handler userId startEventId () =
  lwt nodes = Events_db.event_chain startEventId in

  let cell_width = 230 in
  let make_cell o x y =
    div ~a:[ a_style @@ sprintf "top: %dpx; left: %dpx;" (cell_width*y) (cell_width*x)
	   ; a_class ["tree_node_holder"] ]
      [ div ~a:[a_class ["tree_node_text"]]           [pcdata o.Event.title]
      ; a ~a:[a_class ["tree_node_more_info_link"]] ~service:Ukraine_services.view_event_service
	  [pcdata "more info"] o.Event.id
      ; a ~a:[a_class ["tree_node_center_link"]] ~service:Ukraine_services.tree_view_service
	  [pcdata "Center"] o.Event.id
      ]
  in
  let content_div =
    div
      [ pcdata "depth = 4"
      ; br ()
      ; div ~a:[ a_class ["tree_nodes_holder"]; a_style @@ sprintf "height: %dpx;" cell_width]
	  (List.mapi nodes ~f:(fun i o -> make_cell o i 0))
      ]
  in

  Lwt.return (Ukraine_container.page ~user:userId  [
    content_div
  ])



let () =
  Ebapp.App.register
    (Ukraine_services.tree_view_service)
    (Ebapp.Page.connected_page ~fallback:LoginError.page tree_view_handler)



