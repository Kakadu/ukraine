(* There you will find page for full description of node in timeline.
 * Below the description some space for user's comments to the node
 * should be.
 *)

{shared{
  open Eliom_content.Html5
  open Eliom_content.Html5.F
  open Printf
  open Helpers
  open Types
}}

{client{
  open Ojquery

  let _bad = fun id -> Firebug.console##log (Js.string @@ sprintf "Can't get element by id '%s'" id)
  let prompt_line ?text ~title ~ok =
    let ans = Dom_html.window##prompt
	(Js.string title, Js.string @@ match text with Some x -> x | None -> "") in
    Js.Opt.iter ans (fun text -> ok (Js.to_string text))

}}
(*
let dummy_add_comment_service =
  Eliom_service.Http.post_coservice'
    ~name:"set_data"
    ~post_params:(Eliom_parameter.(int64 "id" ** string "text"))
    ()

{shared{
let node_comments_real_holder_id = "node_comments_real_holder_id"
let node_description_holder_id   = "node_description_holder"
}}

let add_comment (userId, nodeId, commentId, text) =
  match commentId with
  | Some parentId ->
    printf "adding comment by %Ld to node %Ld as subcomment of %Ld with text '%s'\n%!"
      userId nodeId parentId text;
    lwt () = Comments_db.insert_subcomment ~text ~nodeId ~parentId ~userId in
    Lwt.return true
  | None          ->
    lwt () = Comments_db.insert_comment ~text ~nodeId ~userId in
    Lwt.return true

let add_comment_rpc : (userId_t*eventId_t*(commentId_t option)*string, bool) Eliom_pervasives.server_function
  = server_function Json.t<userId_t*eventId_t*(commentId_t option)*string> add_comment

let filter_ans xs id =
  let open StdLabels in
  let cur = List.find ~f:(fun o -> o.Node.id = id) xs in
  List.filter xs ~f:(fun o ->
    (List.length o.Node.path = 1 + List.length cur.Node.path)
  )

{client{
let refresh_comments_data () =
  Js.Opt.case (Dom_html.document##getElementById (Js.string "refresh_comments_btn") )
    (fun () -> Firebug.console##log (Js.string "no refresh_comments_btn"))
    (fun ddd -> ddd |> jQelt |> click |> ignore);
}}

let make_comment_div ~userId ~nodeId padding (o: Comment.t) =
  let open Eliom_content.Html5.D in
  let comment_id: int64 = o.Comment.id in
  let comment_div_id = sprintf "node-comment-%Ld" comment_id in

  let answer_form_div = div ~a:[a_class ["node_comment_reply_block"]; a_style "display: none;"]
    [ post_form ~service:dummy_add_comment_service (fun (id,text) ->
       let area = textarea ~a:[a_class ["node_comment_reply_textarea"]] ~name:text
		  ~value:"comment text here" () in
       let add_comment_onclick = {Dom_html.mouseEvent Js.t -> unit { fun _event ->
          Dom.preventDefault _event;
          let jsarea = To_dom.of_textarea %area in
	  Firebug.console##log (jsarea##value);
	  begin
	  lwt res = %add_comment_rpc (%userId, %nodeId, Some %comment_id, Js.to_string jsarea##value) in
          refresh_comments_data ();
	  Lwt.return ()
	  end |> Lwt.ignore_result
       }} in
       [ area
       ; int64_input ~input_type:`Hidden ~name:id ~value:comment_id ()
       ; raw_input ~a:[a_onclick add_comment_onclick]
		   ~input_type:`Submit ~value:"Comment" ()
       ]) ()
    ]
  in
  let reply_clicked  = {Dom_html.mouseEvent Js.t -> unit { fun _ ->
    Firebug.console##log (Js.string @@ sprintf "reply on comment %Ld" %comment_id);
    Js.Opt.case (Dom_html.document##getElementById (Js.string %comment_div_id) )
		(fun () -> Firebug.console##log (Js.string "WTF"))
     (fun ddd ->
       Manip.SetCss.display %answer_form_div "block";
       Firebug.console##log (Js.string @@ sprintf "reply on comment %Ld" %comment_id);
     )
  }} in

  div ~a: [ a_style @@ sprintf "margin-left: %dpx;" (30*padding); a_class ["node_comment"]
	  ; a_id comment_div_id
	  ]
      [ div ~a:[a_class ["node_comment_holder"]] [
	    (* comment static data here *)
	    div ~a:[a_class ["node_comment_header"]]
		[ div ~a:[a_class ["node_comment_avatar"]]
		      [ img ~src:(Xml.uri_of_string Consts.no_image_url) ~alt:"alt" () ]
		; div ~a:[a_class ["node_comment_nick"]] [pcdata "nickname"]
		]
	  ; div ~a:[a_class ["node_comment_text"]]  [ pcdata o.Comment.text ]
	  ; div ~a:[a_class ["button";"node_comment_reply_btn"]; a_onclick reply_clicked]
		[pcdata "Reply"]
	  ]
      ; answer_form_div
      ]

let generate_comments_div (userId, nodeId) : _ elt list Lwt.t =
  lwt comments = Comments_db.comments_for_node nodeId in
  let (tree, non_parsed_part) = Path.make_tree_of_nodes (fun o -> o.Comment.path) comments in

  let rec to_divs acc depth : Types.Comment.t Path.tree -> _ elt list = function
    | Path.Node (x,xs) ->
       let r = make_comment_div ~userId ~nodeId depth x in
       List.fold_left ~init:(r::acc) xs ~f:(fun acc node -> to_divs acc (depth+1) node )
  in
  Lwt.return @@ ( List.concat (List.map (List.rev tree) ~f:(to_divs [] 0 ) ) |> List.rev  )

let generate_comments_div_rpc
    : (userId_t*eventId_t, [Html5_types.div] elt list) Eliom_pervasives.server_function
  = server_function Json.t<userId_t*eventId_t> generate_comments_div

let make_comments_holder (userId: Int64.t) nodeId  =
  lwt comment_divs = generate_comments_div (userId, nodeId) in
  let _ = Int64.to_int userId in
  let node_comments_real_holder =
    let open Eliom_content.Html5.D in
    div ~a:[a_class ["node_comments_real_holder"]; a_id node_comments_real_holder_id] comment_divs
  in
  let refresh_onclick = {Dom_html.mouseEvent Js.t -> unit { fun _ ->
    Firebug.console##log (Js.string "clicked");
    Lwt.ignore_result (
	lwt divs = %generate_comments_div_rpc (%userId,%nodeId) in
	Firebug.console##log (Js.string @@ string_of_int @@ List.length divs);
	Manip.removeAllChild %node_comments_real_holder;
	Manip.appendChilds %node_comments_real_holder divs;
	Lwt.return ()
      )
  }} in
  let refresh_button = div ~a:[a_class ["button"]; a_id "refresh_comments_btn"; a_onclick refresh_onclick ]
			   [pcdata "Refresh"] in
  Lwt.return @@
    div ~a:[a_class ["node_comments_holder"]]
	[div ~a:[a_class ["node_comments_holder1"]] [refresh_button]
	(* some buttons there *)
	; hr ()
	; node_comments_real_holder
	]

let questions_and_options nodeId =
  Events_db.questions_with_options_of_nodeId nodeId

let questions_and_options_rpc
    : (eventId_t, Types.Question.t list) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t> questions_and_options

let get_node_by_id_rpc : (eventId_t, Types.Event.t) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t> Events_db.event_by_id

let remove_question_rpc: (questionId_t, bool) Eliom_pervasives.server_function
  = server_function Json.t<questionId_t> (fun questionId -> Events_db.remove_question ~questionId)

let add_option_rpc: (optionId_t*string, bool) Eliom_pervasives.server_function
  = server_function Json.t<optionId_t*string>
    (fun (questionId,text) -> Events_db.insert_option ~questionId ~text)

let remove_option_rpc: (optionId_t, bool) Eliom_pervasives.server_function
  = server_function Json.t<optionId_t> (fun optionId -> Events_db.remove_option ~optionId)

{client{

let make_question_div ~refresh (nodeId, q) =
  let remove_question_onclicked = fun _event ->
    Dom.preventDefault _event;
    Firebug.console##log (Js.string "remove_question_onclicked");
    begin
    lwt (_:bool) = %remove_question_rpc q.Question.id in
    let () = refresh nodeId in
    Lwt.return ()
    end |> Lwt.ignore_result
  in

  let add_option_clicked = fun _event ->
      Dom.preventDefault _event;
      Firebug.console##log (Js.string "add_option_onclicked");
      prompt_line ~title:"Enter new option text" ~text:"" ~ok:begin fun text ->
	Firebug.console##log (Js.string @@ sprintf  "your option value =  '%s'" text);
        (lwt res = %add_option_rpc (q.Question.id, text) in
	 let () = if res then begin
           Firebug.console##log (Js.string "Added new option");
           refresh nodeId;
         end else
           Firebug.console##error (Js.string "Server can't add option ");
         in
         Lwt.return ()
	) |> Lwt.ignore_result
       end;
      ()
  in
  let make_option_div (cid,text) =
    let remove_option_clicked = fun _event ->
      Dom.preventDefault _event;
      Firebug.console##log (Js.string "remove_option_onclicked");
      begin
	lwt (_:bool) = %remove_option_rpc cid in
        let () = refresh nodeId in
        Lwt.return ()
      end |> Lwt.ignore_result
    in

    div ~a:[a_class ["question_option_div"]]
      [ pcdata text
      ; dummy_img ~a:[a_onclick remove_option_clicked;
		      a_class ["remove_option_btn"]; a_title "Remove option"] ()
      ]
  in
  Lwt.return @@
    div ~a:[a_class ["question_div"]]
        [ dummy_img ~a:[a_class ["question_icon_img"]] ()
        ; div ~a:[a_class ["question_div_text"]] [pcdata q.Question.text]
        ; dummy_img ~a:[a_onclick remove_question_onclicked; a_class ["remove_question_btn"];
			a_title "Remove question"] ()
        ; dummy_img ~a:[a_onclick add_option_clicked; a_class ["add_option_btn"]; a_title "Add new option"]()
        ; br ()
        ; div ~a:[a_class ["question_options_div"]] (List.map q.Question.options ~f:make_option_div)
        ; hr ()
        ]

}}

let add_question_rpc: (eventId_t*string, bool) Eliom_pervasives.server_function
  = server_function Json.t<eventId_t*string> (fun (nodeId,text) -> Events_db.insert_question ~nodeId ~text)

{client{
let rec refresh_node_data nodeId = Lwt.ignore_result begin
  Firebug.console##log (Js.string @@ sprintf "refresh_node_data %Ld" nodeId);
  let add_question_clicked = fun _event ->
    Dom.preventDefault _event;
    Firebug.console##log (Js.string "add_question_onclicked");
    prompt_line ~title:"Enter new question" ~text:"" ~ok:begin fun text ->
      Firebug.console##log (Js.string @@ sprintf  "your value %s" text);
      (lwt res = %add_question_rpc (nodeId, text) in
      let () = if res then begin
        Firebug.console##log (Js.string "Added");
        refresh_node_data nodeId;
      end else begin
        Firebug.console##error (Js.string "Server can't add question");
      end in
      Lwt.return ()) |> Lwt.ignore_result
    end
  in

  lwt curnode = %get_node_by_id_rpc nodeId in

  lwt questions = %questions_and_options_rpc nodeId in
  let () = Firebug.console##log (Js.string @@ string_of_int @@ List.length questions) in
  let qdivs = List.map ~f:(fun q -> make_question_div ~refresh:refresh_node_data (nodeId,q)) questions in
  lwt question_divs = Lwt_list.map_p (fun x -> x) qdivs in
  Firebug.console##log (Js.string @@ sprintf "refresh_node_data: q.len = %d" (List.length question_divs));
  with_element_by_id_exn "node_description_holder" ~ok:(fun el ->
      let nodes =
	[ div ~a:[a_class ["node_title"]] [pcdata @@ curnode.Event.title]
	; br ()
	; (let src = Helpers.Option.get ~default:Consts.no_image_url curnode.Event.picture
	      |> Xml.uri_of_string in
          img ~src ~alt:"alt" ())
	; br ()
	; div ~a:[a_class ["node_description"]]
	   [ p [pcdata @@ if String.length curnode.Event.text = 0 then "<no description>" else curnode.Event.text ]
	   ]
        ; hr ()
        (* Button for adding new question *)
        ; div ~a:[a_class ["button"; "add_new_question_btn"]; a_onclick add_question_clicked] [pcdata "Add question"]
        ; div ~a:[a_class ["questions_block"]] question_divs
	]
      in
      Manip.replaceAllChild (el |> Of_dom.of_element) nodes
  ) ~bad:_bad;
  Lwt.return ()
end
}}
					     *)
let make_event_links xs =
  let f ev =
    div [ a ~a:[a_class ["when_view_link"]] ~service:Ukraine_services.view_event_service
	    [pcdata ev.Event.title] ev.Event.id
        ; br ()
	; div ~a:[a_class ["when_view_link_time"]] [pcdata @@ string_of_calendar ev.Event.datetime]
        ; br ()
        ; br ()
	]
  in
  div ~a:[a_class ["when_view_links_holder"]]
    (List.map ~f xs)

let handler tlsort (userId: userId_t) (yy,(mm,dd)) () =
  lwt events = Events_db.by_date tlsort ~yy ~dd ~mm in

(*
  lwt curnode = Events_db.event_by_id nodeId  in
  lwt comments_holder = make_comments_holder userId nodeId in

  let onload_node_description_div = { Dom_html.event Js.t -> unit { fun _ ->
      refresh_node_data %nodeId;
  }} in
  let refresh_node_data_btn_clicked = { Dom_html.mouseEvent Js.t -> unit { fun _event ->
      Dom.preventDefault _event;
      refresh_node_data %nodeId
  }} in
  let content_div = div ~a:[a_class ["node_data_holder"]]
    [ div ~a:[a_onclick refresh_node_data_btn_clicked; a_id "refresh_node_data_btn"]
        [img ~src:(Xml.uri_of_string "") ~alt:"" () ]
    ; div ~a:[a_class ["node_description_holder"]; a_id node_description_holder_id; a_onload onload_node_description_div] []
    ; div ~a:[a_class ["node_buttons"]]
      [ div ~a:[a_class ["node_fork_button"]]
	  [ a ~a:[a_class ["item"; "left-bar"]]
		~service:Ukraine_services.add_node_service
		[ pcdata "Add branch"
		] curnode.Event.id
	  ; br ()
	  ]
      ; br()
      ; a ~a:[a_class ["item"; "left-bar"]]
	  ~service:Ukraine_services.tree_view_service
	  [ pcdata "View tree from there"
	  ] curnode.Event.id
      ; br ()
      ]
    ; br ()
    ; comments_holder
    ; br ()
    ; div ~a:[a_class ["node_comment_root_reply_block"]]
	  [ post_form ~service:dummy_add_comment_service (fun (id,text) ->
	      let open Eliom_content.Html5.D in
	      let area = textarea ~a:[a_class ["node_comment_root_reply_textarea"]]
				  ~name:text  ~value:"comment text here" () in
	      let add_comment_onclick = {Dom_html.mouseEvent Js.t -> unit { fun _event ->
		Dom.preventDefault _event;
		let jsarea = To_dom.of_textarea %area in
	        Firebug.console##log (jsarea##value);
		begin
		lwt res = %add_comment_rpc (%userId, %nodeId, None, Js.to_string jsarea##value) in
                if res then (
		  Js.Opt.case (Dom_html.document##getElementById (Js.string %node_comments_real_holder_id) )
			      (fun () -> Firebug.console##log (Js.string "WTF"); Lwt.return ())
		  (fun ddd ->
                     lwt divs = %generate_comments_div_rpc (%userId,%nodeId) in
		     let holder = Of_dom.of_div ddd in
                     Firebug.console##log (Js.string@@ string_of_int @@ List.length divs);
		     Manip.removeAllChild holder;
		     Manip.appendChilds holder divs;
		     Firebug.console##log (Js.string "refreshed");
		     Lwt.return ()
		  )
		) else (
		  Firebug.console##log (Js.string "WTF");
		  Lwt.return ()
                ) end |> Lwt.ignore_result
	      }} in
	      [ hr ()
	      ; div ~a:[a_style "float: left;"] [pcdata "Comment node:"]
	      ; br ()
	      ; area
	      ; br ()
	      ; raw_input ~a:[a_onclick add_comment_onclick; a_class ["node_comment_root_submit_btn"]]
			  ~input_type:`Submit ~value:"Comment" ()
	      ]) ()
	   ]
    ]
    in
 *)
  let content_div = div ~a:[a_class ["when_view_content_div"]]
    [ a ~a:[a_class ["when_view_back_btn"]] ~service:Ukraine_services.view_date_service
	[pcdata "Back"] (yy,(mm,dd-1))
    (* FIXME don't do +1 and -1 *)
    ; div ~a:[a_class ["when_view_today_label"]] [pcdata @@ sprintf "Today: %d/%d/%d" yy mm dd]
    ; a ~a:[a_class ["when_view_forward_btn"]] ~service:Ukraine_services.view_date_service
	[pcdata "Forward"] (yy,(mm,dd+1))
    ; hr ()
    ; make_event_links events
    ]
  in
  Lwt.return (Ukraine_container.page ~user:userId [
    content_div
  ])

let () =
  let handler = handler Maidan2013 in
  Ebapp.App.register Ukraine_services.view_date_service
    (Ebapp.Page.connected_page ~fallback:LoginError.page handler)
