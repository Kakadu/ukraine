{shared{
open Printf
open Eliom_content.Html5
open Eliom_content.Html5.F
}}

let rec main_service_fallback uid gp pp exc =
  let open Ebapp.Page in
  match exc with
  | Ebapp.Session.Not_connected ->
      (** The following correspond to the home page on when disconnected. *)
      Lwt.return (Ukraine_container.page [
        div ~a:[a_id "ukraine-forms"] [
          div ~a:[a_class ["left-bar"]]
          [ b [pcdata "Login with OpenId"]

          ; b [pcdata "Sign in:"];
            hr ();
            p [pcdata "You can sign in if you have already an account:"];
            Ukraine_view.connect_form ();
            a ~service:Ukraine_services.forgot_password_service [
              pcdata "Forgot your password?";
            ] ();
          ];
          p [b [pcdata "OR"]];
          div ~a:[a_class ["left-bar"]] [
            b [pcdata "Sign up:"];
            hr ();
            p [pcdata "Just sign up to our awesome application!"];
            Ukraine_view.sign_up_form ();
          ];
          p [b [pcdata "OR"]];
          div ~a:[a_class ["left-bar"]] [
            b [pcdata "Preregister:"];
            hr ();
            p [
              pcdata "If you are interested by our application,";
              pcdata " please let us your email address!";
            ];
            Ukraine_view.preregister_form ();
          ];
        ];
        div ~a:[a_class ["clear"]] [];
      ])
  | _ -> Lwt.return (Ukraine_container.page [])

let  main_service_handler uid gp pp =
  let open Ukraine_user in
  lwt user = Ukraine_user.user_of_uid uid in
  printf "main_service_handler for %s\n%!" user.fn;
  (* If user is OK we redirect to current date in timeline
   * else we show form about updating user *)
  let redirect_onload = {Dom_html.event Js.t -> unit { fun _event ->
    let today = jsnew Js.date_now () in
    let day   = today##getDate () in
    let month = 1 + today##getMonth () in (* numeration stats from 0*)
    let year  = today##getFullYear () in
    Eliom_client.exit_to ~service: %Ukraine_services.view_date_service (year,(month,day)) ()
  }} in
  Lwt.return (Ukraine_container.page ~user (
    if (user.fn = "" || user.ln = "")
    then [ Ukraine_view.information_form () ]
    else [
      div ~a:[a_onload redirect_onload] [pcdata "Redirecting to today events...."]
(*
      img ~alt:"no photo" ~a:[a_class ["ukraine-avatar"]]
        ~src:(Ukraine_user.avatar_uri_of_user user)
        (); *)
    ];
  ))

let () =
  let handler mode gp pp =
    match mode with
    | `GuestConnected
    | `GuestDenied -> main_service_fallback () gp pp Ebapp.Session.Not_connected

    | `Allowed uid
    | `Denied  uid  -> main_service_handler uid gp pp
  in
  Ebapp.App.register Ukraine_services.main_service
    (Ebapp.Page.how_connected_page ~onerror:(LoginError.page None) handler) (*
       ~fallback:main_service_fallback
       main_service_handler)
                                             *)


let set_personal_data_handler' uid ()
    (((firstname, lastname), (pwd, pwd2)) as pd) =
  if firstname = "" || lastname = "" || pwd <> pwd2
  then
    (Ukraine_reqm.(set wrong_pdata pd);
     Lwt.return ())
  else (
    lwt user = Ukraine_user.user_of_uid uid in
    let open Ukraine_user in
    let record = {
      user with
      fn = firstname;
      ln = lastname;
    } in
    Ukraine_user.update' ~password:pwd record)

let generate_act_key
    ?(act_key = Ocsigen_lib.make_cryptographic_safe_string ())
    ?(send_email = true)
    ~service
    email =
  printf "Inside '%s'\n" "generate_act_key" ;
  let service =
    Eliom_service.attach_coservice' ~fallback:service
      ~service:Ukraine_services.activation_service
  in
  let act_key' = F.make_string_uri ~absolute:true ~service act_key in
  print_endline act_key';
  (if send_email then try
       Ebapp.Email.send
         ~to_addrs:[(email, "")]
         ~subject:"creation"
         [
           "you activation key: "; act_key'; "do not reply";
         ]
     with _ -> ());
  act_key

let sign_up_handler' () email =
  print_endline "sign_up_handler'";
  lwt is_registered = Ukraine_user.is_registered email in
  if is_registered then begin
    ignore (Ukraine_reqm.(error_string "This user already exists"));
    Lwt.return ()
  end else begin
    let act_key = generate_act_key ~service:Ukraine_services.main_service email in
    lwt uid = Ukraine_user.create ~firstname:"" ~lastname:"" email in
    lwt () = Ukraine_user.add_activationkey ~act_key uid in
    Lwt.return ()
  end

let forgot_password_handler () () =
  Lwt.return (Ukraine_container.page [
    div ~a:[a_id "ukraine-forms"] [
      div ~a:[a_class ["left-bar"]] [
        p [pcdata "Enter your email to get an activation link to access to your account!"];
        Ukraine_view.forgot_password_form ();
      ];
    ]
  ])

let forgot_password_handler' () email =
  try_lwt
    lwt uid = Ukraine_user.uid_of_email email in
    let act_key = generate_act_key ~service:Ukraine_services.main_service email in
    Ukraine_user.add_activationkey ~act_key uid
  with Ukraine_db.No_such_resource ->
    let (_: _ Ebapp.Reqm.reqm) = Ukraine_reqm.(error_string "This user does not exists") in
    Lwt.return ()

let about_handler () () =
  Lwt.return (Ukraine_container.page [
    div [
      p [pcdata "This template provides you a skeleton for an ocsigen application."];
      hr ();
      p [pcdata "Feel free to modify the code."]
    ]
  ])

let disconnect_handler () () =
  (* SECURITY: no check here because we disconnect the session cookie owner. *)
  print_endline "disconnect_handler";
  lwt () = Ebapp.Session.disconnect () in
  lwt () = Eliom_state.discard ~scope:Eliom_common.default_session_scope () in
  lwt () = Eliom_state.discard ~scope:Eliom_common.default_process_scope () in
  lwt () = Eliom_state.discard ~scope:Eliom_common.request_scope () in
  Lwt.return ()

let connect_handler () (login, pwd) =
  (* SECURITY: no check here. *)
  lwt () = disconnect_handler () () in
  try_lwt
    lwt uid = Ukraine_user.verify_password login pwd in
    Printf.printf "connecting user %s\n%!" (Int64.to_string uid);
    lwt () = Ebapp.Session.connect uid in
    let () =
      try ignore (Ebapp.Session.get_current_userid ())
      with Eba_shared.Session.Not_connected -> print_endline "ERRROR"
    in
    Lwt.return ()
  with Ukraine_db.No_such_resource ->
    let (_: _ Ebapp.Reqm.reqm) = Ukraine_reqm.(error_string "Your password does not match.") in
    Lwt.return ()

let activation_handler akey () =
  (* SECURITY: we disconnect the user before doing anything
   * moreover in this case, if the user is already disconnect
   * we're going to disconnect him even if the actionvation key
   * is outdated. *)
  printf  "activation_handler '%s'\n%!" akey;
  lwt () = Ebapp.Session.disconnect () in
  try_lwt
    lwt uid = Ukraine_user.uid_of_activationkey akey in
    lwt () = Ebapp.Session.connect uid in
    Eliom_registration.Redirection.send Eliom_service.void_coservice'
  with Ukraine_db.No_such_resource ->
    let (_: _ Ebapp.Reqm.reqm) = Ukraine_reqm.(notice_string "An activation key has been created") in
    Eliom_registration.Action.send ()

          (*
let admin_service_handler uid gp pp =
  lwt user = Ukraine_user.user_of_uid uid in
  (*lwt cnt = Ebapp.Admin.admin_page_content user in*)
  Lwt.return (Ukraine_container.page [
  ] (*@ cnt*) )
           *)

let preregister_handler' () email =
  lwt is_preregistered = Ukraine_user.is_preregistered email in
  lwt is_registered = Ukraine_user.is_registered email in
  Printf.printf "%b:%b%!\n" is_preregistered is_registered;
  if not (is_preregistered || is_registered)
   then Ukraine_user.add_preregister email
   else begin
     ignore (Ukraine_reqm.(error_string "Email already uses"));
     Lwt.return ()
   end

let () =
  Ebapp.App.register
    (Ukraine_services.forgot_password_service)
    (Ebapp.Page.page
       forgot_password_handler);

  Ebapp.App.register
    (Ukraine_services.about_service)
    (Ebapp.Page.page
       about_handler);

  Eliom_registration.Action.register
    (Ukraine_services.set_personal_data_service')
    (Ebapp.Session.connected_fun
       set_personal_data_handler');

  Eliom_registration.Action.register
    (Ukraine_services.forgot_password_service')
    (forgot_password_handler');

  Eliom_registration.Action.register
    (Ukraine_services.preregister_service')
    (preregister_handler');

  Eliom_registration.Action.register
    (Ukraine_services.sign_up_service')
    (sign_up_handler');

  Eliom_registration.Action.register
    (Ukraine_services.connect_service)
    (connect_handler);

  Eliom_registration.Action.register
    (Ukraine_services.disconnect_service)
    (disconnect_handler);

  Eliom_registration.Any.register
    (Ukraine_services.activation_service)
    (activation_handler)
